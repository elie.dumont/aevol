#!/usr/bin/env bash

###############################################################################
# $1: the ctest directory
# $2: whether to run the test (TEST) or generate ref data (GENERATE_REF)
# $3: the current test's name
# $4: the current test's parameter file
# $5: the current test's binding matrix file (if any)
# $6: dummy parameter
# $7: the aevol_create binary to be tested
###############################################################################

CTEST_DIR=$1
TEST_OR_GENERATE=$2
CUR_TEST_FULL_NAME=$3
CUR_TEST_PARAM_FILE=$4
CUR_TEST_BM_FILE=$5
# $6 is dummy param
AEVOL_CREATE=$7

CUR_TEST_DIR=$CTEST_DIR/$CUR_TEST_FULL_NAME

# cd into the test's directory (create if not exists)
if [ ! -e "$CUR_TEST_DIR" ]; then mkdir "$CUR_TEST_DIR" || exit 1; fi
cd "$CUR_TEST_DIR" || exit 1

if [ "$TEST_OR_GENERATE" = "TEST" ]; then
  RESDIR="res"
elif [ "$TEST_OR_GENERATE" = "GENERATE_REF" ]; then
  RESDIR="ref"
else
  echo "passed \"$TEST_OR_GENERATE\"; should be either \"TEST\" or \
      \"GENERATE_REF\""
  exit 1
fi

# cd into $RESDIR (create if not exists)
if [ ! -e "$RESDIR" ]; then mkdir "$RESDIR"; fi
rm -rf "${RESDIR:?}/"*
cd "$RESDIR" || exit 1

# Copy binding matrix if any
if [ "$CUR_TEST_BM_FILE" != "NONE" ]; then
  cp "$CUR_TEST_BM_FILE" "binding_matrix.rae" || exit 1
fi

# run $AEVOL_CREATE
CMD="$AEVOL_CREATE -f $CUR_TEST_PARAM_FILE"
echo "running $CMD from $PWD..."
$CMD || exit 1

# cd back into the test's directory
cd "$CUR_TEST_DIR" || exit 1

if [ "$TEST_OR_GENERATE" = "TEST" ]; then
  # check results vs ref
  fail=0

  # for each regular file in ref, diff it with its res counterpart
  for reffile in $(find ref -type f); do
    # Strip reffile of the first dir level ("ref")
    file=${reffile#ref/} # Shell Parameter Expansion
    echo "checking file $file"
    if ! diff -q ref/$file res/$file; then
      fail=1
    fi
  done

  if test $fail -ne 0; then exit 1; fi
fi

exit 0
