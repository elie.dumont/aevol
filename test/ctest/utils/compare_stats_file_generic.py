#!/usr/bin/env python
# coding: utf-8

import argparse
import pandas as pd
from pathlib import Path
import sys

def main():
  # Get command line arguments
  parser = argparse.ArgumentParser(description='Compare stats files')
  parser.add_argument(
      'fileA', type=Path,
      help='first file to compare')
  parser.add_argument(
      'fileB', type=Path,
      help='second file to compare')

  args = parser.parse_args()

  # Compare files
  compare(args.fileA, args.fileB)

  sys.exit(0)


def compare(fileA: Path, fileB: Path):
  dfA = pd.read_csv(fileA, comment='#', sep=' ')
  dfB = pd.read_csv(fileB, comment='#', sep=' ')

  # Assert that frames are equal
  # assert_frame_equal internally uses numpy.isclose with rtol and atol (relative and absolute tolerance)
  # parameters rtol and atol are explicitly set to their default values for future reference
  try:
    pd.testing.assert_frame_equal(dfA, dfB, rtol=1e-5, atol=1e-8)
  except AssertionError as e:
    print(e)
    sys.exit(1)


if __name__ == '__main__':
  main()
