# Add gtest tests
add_subdirectory(gtest)

# Add Ctest tests
add_subdirectory(ctest)
