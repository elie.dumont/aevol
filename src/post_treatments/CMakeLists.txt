# ============================================================================
# List aevol-flavour-independent post_treatments
# ============================================================================
set(AEVOL_POST_TREATMENTS
    post_create_eps
    post_create_eps_7
    post_create_csv_7
    post_extract
    post_extract_genome_structure
    post_ancestor_stats
    post_ancestor_stats_7
    post_ancestor_mutagenesis
    post_coalescence
    post_ancestor_robustness
    post_lineage
    post_muller
    post_mutagenesis
    post_robustness
    # post_robustness_json
    post_ancestor_extract
    # post_neutral_mut_acc
    # post_change_size_neutral_mut
    # post_neutral_shuffle_genome
    # post_robustness_bias
    )

# ============================================================================
# Tell cmake about subdirectories to look into
# ============================================================================
# libaevol-post contains classes shared by several post-treatments
add_subdirectory(libaevol_post)

# ============================================================================
# Add targets add their dependencies
# ============================================================================
foreach (AEVOL_FLAVOUR IN LISTS AEVOL_FLAVOURS)
  foreach (AEVOL_POST_TREATMENT IN LISTS AEVOL_POST_TREATMENTS)
    add_executable(${AEVOL_FLAVOUR}_${AEVOL_POST_TREATMENT}
        aevol_${AEVOL_POST_TREATMENT}.cpp)
    target_link_libraries(${AEVOL_FLAVOUR}_${AEVOL_POST_TREATMENT}
        PUBLIC lib${AEVOL_FLAVOUR})
    add_dependencies(${AEVOL_FLAVOUR}
        ${AEVOL_FLAVOUR}_${AEVOL_POST_TREATMENT})
  endforeach (AEVOL_POST_TREATMENT)

  # Link those post-treatments that require it vs libaevol-post
  target_link_libraries(${AEVOL_FLAVOUR}_post_ancestor_mutagenesis
      PUBLIC lib${AEVOL_FLAVOUR}-post)
  target_link_libraries(${AEVOL_FLAVOUR}_post_ancestor_robustness
      PUBLIC lib${AEVOL_FLAVOUR}-post)
  target_link_libraries(${AEVOL_FLAVOUR}_post_robustness
      PUBLIC lib${AEVOL_FLAVOUR}-post)
#  target_link_libraries(${AEVOL_FLAVOUR}_post_robustness_json
#      PUBLIC lib${AEVOL_FLAVOUR}-post)
  target_link_libraries(${AEVOL_FLAVOUR}_post_ancestor_extract
      PUBLIC lib${AEVOL_FLAVOUR}-post)
#  target_link_libraries(${AEVOL_FLAVOUR}_post_neutral_mut_acc
#      PUBLIC lib${AEVOL_FLAVOUR}-post)
#  target_link_libraries(${AEVOL_FLAVOUR}_post_change_size_neutral_mut
#      PUBLIC lib${AEVOL_FLAVOUR}-post)
#  target_link_libraries(${AEVOL_FLAVOUR}_post_neutral_shuffle_genome
#      PUBLIC lib${AEVOL_FLAVOUR}-post)
#  target_link_libraries(${AEVOL_FLAVOUR}_post_robustness_bias
#      PUBLIC lib${AEVOL_FLAVOUR}-post)
endforeach (AEVOL_FLAVOUR)

# RAevol-specific
add_executable(raevol_post_ancestor_network aevol_post_ancestor_network.cpp)
target_link_libraries(raevol_post_ancestor_network
  PUBLIC libraevol libraevol-post)
add_dependencies(raevol raevol_post_ancestor_network)

add_executable(raevol_post_ancestor_network_knockout_7
  aevol_post_ancestor_network_knockout_7.cpp)
target_link_libraries(raevol_post_ancestor_network_knockout_7
  PUBLIC libraevol)
add_dependencies(raevol raevol_post_ancestor_network_knockout_7)

# ============================================================================
# Set behaviour on make install
# ============================================================================
