// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PROMOTERLIST_H_
#define AEVOL_PROMOTERLIST_H_

#include <array>
#include <list>

#include "ae_enums.h"
#include "Dna_7.h"
#include "Promoter.h"

namespace aevol {

class PromoterList {
 public:
  using SingleStranded = std::list<Promoter>;
  using DoubleStranded = std::array<SingleStranded, 2>;

  PromoterList();
  PromoterList(const PromoterList&)            = delete;
  PromoterList(PromoterList&&)                 = default;
  PromoterList& operator=(const PromoterList&) = delete;
  PromoterList& operator=(PromoterList&&)      = default;
  virtual ~PromoterList()                      = default;

  inline DoubleStranded& double_stranded_list();
  inline SingleStranded& single_stranded_list(Strand strand);

  inline SingleStranded& operator[](Strand strand);

  Promoter* promoter_at(size_t idx);
  void promoter_add(Promoter&& prom);
  int promoter_count();

  void lst_promoters(Strand strand,
                     Position before_after_btw,  // with regard to the strand's reading direction
                     int32_t pos1,
                     int32_t pos2,
                     SingleStranded& motif_list);

  void remove_promoters_around(int32_t pos_1, int32_t dna_len);
  void remove_promoters_around(int32_t pos_1, int32_t pos_2, int32_t dna_len);
  void remove_all_promoters();

  void look_for_new_promoters_around(const Dna_7& dna, int32_t pos_1, int32_t pos_2);
  void look_for_new_promoters_around(const Dna_7& dna, int32_t pos);

  void locate_promoters(const Dna_7& dna);

  void move_all_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len);

  PromoterList duplicate_promoters_included_in(int32_t pos_1, int32_t pos_2, int32_t dna_len);
  PromoterList extract_promoters_included_in(int32_t pos_1, int32_t pos_2);
  void insert_promoters(PromoterList& promoters_to_insert);
  void insert_promoters_at(PromoterList& promoters_to_insert, int32_t pos, int32_t dna_len);

  void invert_promoters_included_in(int32_t pos1, int32_t pos2);

  void shift_promoters(int32_t delta_pos, int32_t seq_length);
  void invert_promoters(int32_t pos1, int32_t pos2);

  void remove_leading_promoters_starting_between(int32_t pos_1, int32_t pos_2);
  void remove_leading_promoters_starting_after(int32_t pos);
  void remove_leading_promoters_starting_before(int32_t pos);

  void remove_lagging_promoters_starting_between(int32_t pos_1, int32_t pos_2, int32_t dna_len);
  void remove_lagging_promoters_starting_after(int32_t pos);
  void remove_lagging_promoters_starting_before(int32_t pos);

  void move_all_leading_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len);
  void move_all_lagging_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len);

  void look_for_new_leading_promoters_starting_between(const Dna_7& dna, int32_t pos_1, int32_t pos_2);
  void look_for_new_leading_promoters_starting_after(const Dna_7& dna, int32_t pos);
  void look_for_new_leading_promoters_starting_before(const Dna_7& dna, int32_t pos);

  void look_for_new_lagging_promoters_starting_between(const Dna_7& dna, int32_t pos_1, int32_t pos_2);
  void look_for_new_lagging_promoters_starting_after(const Dna_7& dna, int32_t pos);
  void look_for_new_lagging_promoters_starting_before(const Dna_7& dna, int32_t pos);

  PromoterList promoters_included_in(int32_t pos_1, int32_t pos_2, int32_t dna_len);

  void extract_leading_promoters_starting_between(
      int32_t pos_1, int32_t pos_2, SingleStranded& extracted_promoters);

  void extract_lagging_promoters_starting_between(
      int32_t pos_1, int32_t pos_2, SingleStranded& extracted_promoters);

 protected:
  DoubleStranded double_stranded_list_ = {{{}, {}}};
};

PromoterList::DoubleStranded& PromoterList::double_stranded_list() {
  return double_stranded_list_;
}

PromoterList::SingleStranded& PromoterList::single_stranded_list(Strand strand) {
  return double_stranded_list_[to_underlying(strand)];
}

PromoterList::SingleStranded& PromoterList::operator[](Strand strand) {
  return single_stranded_list(strand);
}

}  // namespace aevol

#endif  // AEVOL_PROMOTERLIST_H_
