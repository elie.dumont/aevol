// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#include "Individual_7.h"
#include "DnaMutator.h"
#include "Stats_7.h"
#include "7/List_Metadata.h"
#include "7/Protein_7.h"

#include "legacy/phenotype/fuzzy/Fuzzy.h"

#include <algorithm>

namespace aevol {

/** Individual_7 Constructor and Destructor **/
Individual_7::Individual_7(double w_max, DnaFactory* dna_factory, FuzzyFactory_7* fuzzy_factory) {
        w_max_ = w_max;
        usage_count_ = 1;

        metadata_ = new List_Metadata(this);

        dna_factory_ = dna_factory;
        fuzzy_factory_ = fuzzy_factory;
    }

Individual_7::Individual_7(Individual_7* clone,
                           DnaFactory* dna_factory,
                            FuzzyFactory_7* fuzzy_factory,
                           bool no_metadata) {
  w_max_ = clone->w_max_;

  usage_count_ = 1;
  dna_ = dna_factory->get_dna(clone->dna_->length());
  //printf("DNA Factory -- %p %p\n",dna_,dna_->data_);
  dna_->set_indiv(clone->dna_,this);

  dna_factory_ = dna_factory;
  fuzzy_factory_ = fuzzy_factory;


  if (no_metadata) {
    metadata_ = new List_Metadata(this);
  } else {
    metadata_ = new List_Metadata(this, dynamic_cast<List_Metadata*>(clone->metadata_));
  }
  fitness = clone->fitness;
  metaerror = clone->metaerror;

  for (auto& strand: {Strand::LEADING, Strand::LAGGING}) {
    for (auto& rna: clone->promoter_list(strand)) {
      promoter_list(strand).emplace_back(rna);
      promoter_list(strand).back().to_compute_end_ = false;
    }
  }
}

Individual_7::Individual_7(double w_max,
                           char* dna_clone,
                           int32_t dna_length,
                           DnaFactory* dna_factory,
                           FuzzyFactory_7* fuzzy_factory,
                           int32_t* lead_prom_pos,
                           int8_t* lead_prom_error,
                           int32_t lead_prom_size,
                           int32_t* lag_prom_pos,
                           int8_t* lag_prom_error,
                           int32_t lag_prom_size) {
  w_max_ = w_max;

  usage_count_ = 1;
  dna_ = dna_factory->get_dna(dna_length);
  //printf("DNA Factory -- %p %p\n",dna_,dna_->data_);
  dna_->set_indiv(dna_clone,dna_length,this);

  dna_factory_ = dna_factory;
  fuzzy_factory_ = fuzzy_factory;

  metadata_ = new List_Metadata(this);

  for (int i = 0; i < lead_prom_size; i++) {
    promoter_list_.promoter_add(Promoter(lead_prom_pos[i], lead_prom_error[i], Strand::LEADING));
  }

  for (int i = lag_prom_size - 1; i >= 0; i--) {
    promoter_list_.promoter_add(Promoter(lag_prom_pos[i], lag_prom_error[i], Strand::LAGGING));
  }
}

Individual_7::~Individual_7() {
  dna_factory_->give_back(dna_);

  if (phenotype!=nullptr) {
    fuzzy_factory_->give_back(phenotype);
    phenotype = nullptr;
  }

  delete metadata_;

  clearAllObserver();
}

void Individual_7::reset_stats() {
  nb_genes_activ     = 0;
  nb_genes_inhib     = 0;
  nb_func_genes      = 0;
  nb_non_func_genes  = 0;
  nb_coding_RNAs     = 0;
  nb_non_coding_RNAs = 0;

  overall_size_coding_RNAs_ = 0;
  overall_size_non_coding_RNAs_ = 0;

  overall_size_fun_genes_ = 0;
  overall_size_non_fun_genes_ = 0;

  nb_bases_in_0_CDS_ = 0;
  nb_bases_in_0_functional_CDS_ = 0;
  nb_bases_in_0_non_functional_CDS_ = 0;
  nb_bases_in_0_RNA_ = 0;
  nb_bases_in_0_coding_RNA_ = 0;
  nb_bases_in_0_non_coding_RNA_ = 0;
  nb_bases_non_essential_ = 0;
  nb_bases_non_essential_including_nf_genes_ = 0;
  nb_bases_in_neutral_regions_ = 0;
}

/**
 * We need some index for the promoter optimization
 */
void Individual_7::rebuild_index() {
}


void Individual_7::reset_metadata() {
    delete metadata_;
    metadata_ = new List_Metadata(this);
    for (auto& strand: {Strand::LEADING, Strand::LAGGING}) {
      promoter_list(strand).clear();
    }
}


#ifdef BASE_2
void Individual_7::search_start_protein(Rna_7* rna, int32_t pos_1, int32_t pos_2) {
        int32_t dna_length =  dna_->length_;
      char* dna_data = dna_->data_;
      #if defined(__INTEL_COMPILER)
        __declspec(align(dna_data));
      #elif defined(__INTEL_LLVM_COMPILER)
         void* vec_r =  __builtin_assume_aligned(dna_data,64);
      #endif

      int32_t rna_length = rna->length;
      auto strand = rna->strand_;

  if (rna->is_init_) {
        int32_t s_pos = pos_1;
        if (rna_length >= 21) {


          for (int32_t loop_size = 0; loop_size < pos_2-pos_1; loop_size++) {
            #if defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
            __declspec(align(64)) bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
            #else
            bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
            #endif

            int32_t c_pos = s_pos;
            if (strand == Strand::LEADING) {
              c_pos+=loop_size;
              c_pos =
                  c_pos >= dna_length ? c_pos - dna_length
                                      : c_pos;
            } else {
              c_pos-=loop_size;
              c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
            }


            
            if (strand == Strand::LEADING) {
              // Search for Shine Dalgarro + START codon on LEADING
              if (c_pos + 15 < dna_length && c_pos + 15 >= 0) {
                #pragma omp simd
                for (int32_t k = 0; k < 12; k++) {
                  // int32_t k_t = k >= 6 ? k + 4 : k;
                 if (dna_data[c_pos+k] == SHINE_DAL_SEQ_LEAD_7[k])
                  start[k] = true;
                }
                if (dna_data[c_pos+12] == SHINE_DAL_SEQ_LEAD_7[12]) start[12] = true;
              } else {
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos + k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LEAD_7[k_t]) ? true: false;
                }
              }
            } else {
              // Search for Shine Dalgarro + START codon on LAGGING
              if (c_pos - 15 < dna_length && c_pos - 15 >= 0) {
                #pragma omp simd
                for (int32_t k = 0; k < 12; k++) {
                  // int32_t k_t = k >= 6 ? k + 4 : k;
                  if (dna_data[c_pos - k] == SHINE_DAL_SEQ_LAG_7[k])
                    start[k] = true;
                }
                if (dna_data[c_pos - 12] == SHINE_DAL_SEQ_LAG_7[12])
                    start[12] = true;
              } else {
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos - k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LAG_7[k_t]) ? true : false;
                }
              }
            }

            if (start[0] && start[1] && start[2] && start[3] && start[4] && start[5] && start[10] && start[11] && start[12]) {
              rna->start_prot_count_++;
              rna->start_prot.push_back(c_pos);
            }
          }
        }
      }
      }
#endif

void Individual_7::compute_non_coding() {
  if (non_coding_computed_) return;
  non_coding_computed_ = true;

  // printf("Compute non coding\n");

  // Create a table of <genome_length> bools initialized to false (non-coding)
  int32_t genome_length = dna_->length_;

  // Including Shine-Dalgarno, spacer, START and STOP
  bool* belongs_to_CDS;
  bool* belongs_to_functional_CDS;
  bool* belongs_to_non_functional_CDS; // non-functional CDSs are those that have a null area or that lack a kind of codons (M, W or H)

  // Including Promoters and terminators
  bool* belongs_to_RNA;
  bool* belongs_to_coding_RNA;
  bool* belongs_to_non_coding_RNA;

  // Genes + prom + term (but not UTRs)
  bool* is_essential_DNA;
  bool* is_essential_DNA_including_nf_genes; // Adds non-functional genes + promoters & terminators

  bool* is_not_neutral;            // prom + term + everything in between (as opposed to neutral)


  belongs_to_CDS = new bool[genome_length];
  belongs_to_functional_CDS = new bool[genome_length];
  belongs_to_non_functional_CDS = new bool[genome_length];
  belongs_to_RNA = new bool[genome_length];
  belongs_to_coding_RNA = new bool[genome_length];
  belongs_to_non_coding_RNA = new bool[genome_length];
  is_essential_DNA = new bool[genome_length];
  is_essential_DNA_including_nf_genes = new bool[genome_length];
  is_not_neutral = new bool[genome_length];

  memset(belongs_to_CDS, 0, genome_length);
  memset(belongs_to_functional_CDS, 0, genome_length);
  memset(belongs_to_non_functional_CDS, 0, genome_length);
  memset(belongs_to_RNA, 0, genome_length);
  memset(belongs_to_coding_RNA, 0, genome_length);
  memset(belongs_to_non_coding_RNA, 0, genome_length);
  memset(is_essential_DNA, 0, genome_length);
  memset(is_essential_DNA_including_nf_genes, 0, genome_length);
  memset(is_not_neutral, 0, genome_length);


  // Parse protein lists and mark the corresponding bases as coding
  metadata_->protein_begin();
  for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
    Protein_7* prot = metadata_->protein_next();
    #ifdef __REGUL
    if (!prot->signal_ && prot->is_init_) {
    #else
    if (prot->is_init_) {
    #endif
      int32_t first;
      int32_t last;

      

      switch (prot->strand_) {
        case Strand::LEADING:
          first = prot->protein_start-PROT_START_SIZE;
          last = prot->protein_end;
          break;
        case Strand::LAGGING:
          last = prot->protein_start+PROT_START_SIZE;
          first = prot->protein_end;
          break;
        default:
          assert(false); // error: should never happen
      }

      if (first <= last) {
        for (int32_t i = first; i <= last; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
      }
      else {
        for (int32_t i = first; i < genome_length; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
        for (int32_t i = 0; i <= last; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
      }

      // Include the promoter and terminator to essential DNA
      // Mark everything between promoter and terminator as not neutral
      for (Rna_7* rna : prot->rna_list_) {

        if (rna->is_init_) {
          int32_t prom_first;
          int32_t prom_last;
          int32_t term_first;
          int32_t term_last;
          int32_t rna_first;
          int32_t rna_last;

          if (prot->strand_ == Strand::LEADING) {
            prom_first = rna->begin;
            prom_last = Utils::mod(prom_first + PROM_SIZE - 1, genome_length);
            term_last = rna->end;
            term_first = Utils::mod(term_last - TERM_SIZE + 1, genome_length);
            rna_first = prom_first;
            rna_last = term_last;
          }
          else {
            prom_last = rna->begin;
            prom_first = Utils::mod(prom_last - PROM_SIZE + 1, genome_length);
            term_first = rna->end;
            term_last = Utils::mod(term_first + TERM_SIZE - 1, genome_length);
            rna_first = term_first;
            rna_last = prom_last;
          }

          // Let us begin with "non-neutral" regions...
          if (rna_first <= rna_last) {
            for (int32_t i = rna_first;
                i <= rna_last; i++) { is_not_neutral[i] = true; }
          }
          else {
            for (int32_t i = rna_first;
                i < genome_length; i++) { is_not_neutral[i] = true; }
            for (int32_t i = 0; i <= rna_last; i++) { is_not_neutral[i] = true; }
          }

          // ...and go on with essential DNA
          if (prom_first <= prom_last) {
            for (int32_t i = prom_first; i <= prom_last; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
          }
          else {
            for (int32_t i = prom_first; i < genome_length; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
            for (int32_t i = 0; i <= prom_last; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
          }
          //~ printf("\n");

          //~ printf("term ");
          if (term_first <= term_last) {
            for (int32_t i = term_first; i <= term_last; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
          }
          else {
            for (int32_t i = term_first; i < genome_length; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
            for (int32_t i = 0; i <= term_last; i++) {
              //~ printf("%ld ", i);
              if (prot->is_functional) is_essential_DNA[i] = true;
              is_essential_DNA_including_nf_genes[i] = true;
            }
          }
        }
        //~ printf("\n");
        //~ getchar();
      }


      if (prot->is_functional) {
        if (first <= last) {
          for (int32_t i = first; i <= last; i++) {
            belongs_to_functional_CDS[i] = true;
          }
        }
        else {
          for (int32_t i = first; i < genome_length; i++) {
            belongs_to_functional_CDS[i] = true;
          }
          for (int32_t i = 0; i <= last; i++) {
            belongs_to_functional_CDS[i] = true;
          }
        }
      }
      else // degenerated protein
      {
        if (first <= last) {
          for (int32_t i = first; i <= last; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
        }
        else {
          for (int32_t i = first; i < genome_length; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
          for (int32_t i = 0; i <= last; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
        }
      }
    }
  }


  // Parse RNA lists and mark the corresponding bases as coding (only for the coding RNAs)
  // TODO vld: this block cries for refactoring
  metadata_->rna_begin();

  for (int rna_idx = 0; rna_idx <
                        (int)metadata_->rna_count(); rna_idx++) {
      Rna_7* rna =
          metadata_->rna_next();
      if (rna->is_init_) {
        int32_t first;
        int32_t last;

        if (rna->strand_ == Strand::LEADING) {
          first = rna->begin;
          last = rna->end;
        }
        else { // (strand == LAGGING)
          first = rna->end;
          last = rna->begin;
        }

        assert(first < genome_length);
        assert(last < genome_length);
        
        if (first <= last) {
          for (int32_t i = first; i <= last; i++) {
            belongs_to_RNA[i] = true;
          }
        }
        else {
          for (int32_t i = first; i < genome_length; i++)
            belongs_to_RNA[i] = true;
          for (int32_t i = 0; i <= last; i++)
            belongs_to_RNA[i] = true;
        }

        bool is_coding_rna = false;
        metadata_->protein_begin();
        for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
          Protein_7* prot = metadata_->protein_next();
          if (prot->is_init_) {
            for (auto i_rna : prot->rna_list_) {
              if (i_rna->begin == rna->begin && i_rna->end == rna->end && i_rna->strand_ == rna->strand_) {
                is_coding_rna = true;
              }

            }
          }
        }


        if (is_coding_rna) { // coding RNA
          if (first <= last) {
            for (int32_t i = first; i <= last; i++)
              belongs_to_coding_RNA[i] = true;
          }
          else {
            for (int32_t i = first; i < genome_length; i++)
              belongs_to_coding_RNA[i] = true;
            for (int32_t i = 0; i <= last; i++)
              belongs_to_coding_RNA[i] = true;
          }
        }
        else // non coding RNA
        {
          if (first <= last) {
            for (int32_t i = first; i <= last; i++)
              belongs_to_non_coding_RNA[i] = true;
          }
          else {
            for (int32_t i = first; i < genome_length; i++)
              belongs_to_non_coding_RNA[i] = true;
            for (int32_t i = 0; i <= last; i++)
              belongs_to_non_coding_RNA[i] = true;
          }
        }
      }
    }
  

  // Count non-coding bases
  nb_bases_in_0_CDS_ = 0;
  nb_bases_in_0_functional_CDS_ = 0;
  nb_bases_in_0_non_functional_CDS_ = 0;
  nb_bases_in_0_RNA_ = 0;
  nb_bases_in_0_coding_RNA_ = 0;
  nb_bases_in_0_non_coding_RNA_ = 0;
  nb_bases_non_essential_ = 0;
  nb_bases_non_essential_including_nf_genes_ = 0;
  nb_bases_in_neutral_regions_ = 0;

  for (int32_t i = 0; i < genome_length; i++) {
    if (belongs_to_CDS[i] == false) {
      nb_bases_in_0_CDS_++;
    }
    if (belongs_to_functional_CDS[i] == false) {
      nb_bases_in_0_functional_CDS_++;
    }
    if (belongs_to_non_functional_CDS[i] == false) {
      nb_bases_in_0_non_functional_CDS_++;
    }
    if (belongs_to_RNA[i] == false) {
      nb_bases_in_0_RNA_++;
    }
    if (belongs_to_coding_RNA[i] == false) {
      nb_bases_in_0_coding_RNA_++;
    }
    if (belongs_to_non_coding_RNA[i] == false) {
      nb_bases_in_0_non_coding_RNA_++;
    }
    if (is_essential_DNA[i] == false) {
      nb_bases_non_essential_++;
    }
    if (is_essential_DNA_including_nf_genes[i] == false) {
      nb_bases_non_essential_including_nf_genes_++;
    }
    if (is_not_neutral[i] == false) {
      nb_bases_in_neutral_regions_++;
    }
  }

  // printf("nb_bases_in_0_CDS_ %d\n",nb_bases_in_0_CDS_);

  delete[] belongs_to_CDS;
  delete[] belongs_to_functional_CDS;
  delete[] belongs_to_non_functional_CDS;
  delete[] belongs_to_RNA;
  delete[] belongs_to_coding_RNA;
  delete[] belongs_to_non_coding_RNA;
  delete[] is_essential_DNA;
  delete[] is_essential_DNA_including_nf_genes;
  delete[] is_not_neutral;
}

PromoterList& Individual_7::promoter_list() {
  return promoter_list_;
}

PromoterList::SingleStranded& Individual_7::promoter_list(Strand strand) {
  return promoter_list_[strand];
}

}
