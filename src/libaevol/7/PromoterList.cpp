// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "PromoterList.h"

#include <algorithm>

#include "List_Metadata.h"
#include "macros.h"
#include "Utils.h"

namespace aevol {

PromoterList::PromoterList() {
}

Promoter* PromoterList::promoter_at(size_t idx) {
  if (idx >= single_stranded_list(Strand::LEADING).size()) {
    auto it = single_stranded_list(Strand::LAGGING).begin();
    std::advance(it, idx - single_stranded_list(Strand::LEADING).size());
    return &*(it);
  } else {
    auto it = single_stranded_list(Strand::LEADING).begin();
    std::advance(it,idx);
    return &*(it);
  }
}

void PromoterList::promoter_add(Promoter&& prom) {
  if (prom.strand_ == Strand::LEADING) {
    single_stranded_list(Strand::LEADING).push_back(prom);
  } else {
    single_stranded_list(Strand::LAGGING).push_front(prom);
  }
}

int PromoterList::promoter_count() {
  return single_stranded_list(Strand::LEADING).size()+
         single_stranded_list(Strand::LAGGING).size();
}

void PromoterList::lst_promoters(Strand strand,
                                 Position before_after_btw,  // with regard to the strand's reading direction
                                 int32_t pos1,
                                 int32_t pos2,
                                 SingleStranded& motif_list) {
  auto it_begin = single_stranded_list(strand).begin();
  auto it_end   = single_stranded_list(strand).end();

  if (before_after_btw != BEFORE) {
    it_begin = find_if(single_stranded_list(strand).begin(),
                       single_stranded_list(strand).end(),
                       [pos1, strand](Promoter& p) {
                         if (strand == Strand::LEADING) {
                           return p.pos >= pos1;
          } else {
            return p.pos < pos1;
          }
        });
  }

  if (before_after_btw != AFTER) {
    it_end = find_if(it_begin, single_stranded_list(strand).end(), [pos2, strand](Promoter& p) {
      if (strand == Strand::LEADING) {
        return p.pos >= pos2;
      } else {
        return p.pos < pos2;
      }
    });
  }

  SingleStranded promoters_1D;
  for (auto it = it_begin; it != it_end; it++) {
    Promoter prom(*it);
    prom.rna      = nullptr;
    promoters_1D.push_back(std::move(prom));
  }

  motif_list.insert(motif_list.end(), promoters_1D.begin(), promoters_1D.end());
}

void PromoterList::remove_promoters_around(int32_t pos_1, int32_t dna_len) {
  if (dna_len >= PROM_SIZE) {
    remove_leading_promoters_starting_between(Utils::mod(pos_1 - PROM_SIZE + 1, dna_len), pos_1);

    remove_lagging_promoters_starting_between(pos_1, Utils::mod(pos_1 + PROM_SIZE - 1, dna_len), dna_len);
  }
  else {
    remove_all_promoters();
  }
}

void PromoterList::remove_promoters_around(int32_t pos_1, int32_t pos_2, int32_t dna_len) {
  if (Utils::mod(pos_1 - pos_2, dna_len) >= PROM_SIZE) {
    remove_leading_promoters_starting_between(Utils::mod(pos_1 - PROM_SIZE + 1, dna_len), pos_2);

    remove_lagging_promoters_starting_between(pos_1, Utils::mod(pos_2 + PROM_SIZE - 1, dna_len), dna_len);
  } else {
    remove_all_promoters();
  }
}

void PromoterList::remove_all_promoters() {
  single_stranded_list(Strand::LEADING).clear();
  single_stranded_list(Strand::LAGGING).clear();
}

void PromoterList::look_for_new_promoters_around(const Dna_7& dna,
                                                 int32_t pos_1,
                                                 int32_t pos_2) {
  if (dna.length() >= PROM_SIZE) {
    look_for_new_leading_promoters_starting_between(dna, Utils::mod(pos_1 - PROM_SIZE + 1, dna.length()), pos_2);
    look_for_new_lagging_promoters_starting_between(dna, pos_1, Utils::mod(pos_2 + PROM_SIZE - 1, dna.length()));
  }
}

void PromoterList::look_for_new_promoters_around(const Dna_7& dna, int32_t pos) {
  if (dna.length() >= PROM_SIZE) {
    look_for_new_leading_promoters_starting_between(dna, Utils::mod(pos - PROM_SIZE + 1, dna.length()), pos);
    look_for_new_lagging_promoters_starting_between(dna, pos, Utils::mod(pos + PROM_SIZE - 1, dna.length()));
  }
}

void PromoterList::locate_promoters(const Dna_7& dna) {
  // Empty list
  for (auto& strand: double_stranded_list_) {
    strand.clear();
  }

  if (dna.length() < PROM_SIZE) {
    return;
  }

  for (int32_t i = 0; i < dna.length(); i++) {
    int8_t dist = List_Metadata::is_promoter_leading(dna, i);
    if (dist <= PROM_MAX_DIFF) {// dist takes the hamming distance of the sequence from the consensus
      single_stranded_list(Strand::LEADING).emplace_back(i, dist, Strand::LEADING);
    }

    dist = List_Metadata::is_promoter_lagging(dna, i);
    if (dist <= PROM_MAX_DIFF) {
      single_stranded_list(Strand::LAGGING).emplace_back(dna.length() - i - 1, dist, Strand::LAGGING);
    }
  }
}

void PromoterList::move_all_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len) {
  move_all_leading_promoters_after(pos, delta_pos, dna_len);
  move_all_lagging_promoters_after(pos, delta_pos, dna_len);
}

PromoterList PromoterList::duplicate_promoters_included_in(int32_t pos_1, int32_t pos_2, int32_t dna_len) {
  // 1) Get promoters to be duplicated
  PromoterList retrieved_promoters = promoters_included_in(pos_1, pos_2, dna_len);

  // 2) Set RNAs' position as their position on the duplicated segment
  PromoterList duplicated_promoters;
  for (auto& strand: {Strand::LEADING, Strand::LAGGING}) {
    for (auto& rna: retrieved_promoters[strand]) {
      // Make a copy of current RNA inside container
      duplicated_promoters[strand].emplace_back(rna);

      // Set RNA's position as it's position on the duplicated segment
      duplicated_promoters[strand].back().pos = Utils::mod(duplicated_promoters[strand].back().pos -pos_1, dna_len);
      duplicated_promoters[strand].back().to_compute_end_ = true;
      duplicated_promoters[strand].back().rna = nullptr;
    }
  }

  return duplicated_promoters;
}

PromoterList PromoterList::extract_promoters_included_in(int32_t pos_1, int32_t pos_2) {
  PromoterList extracted_promoters;

  if (pos_2 - pos_1 < PROM_SIZE) {
    return extracted_promoters;
  }

  extract_leading_promoters_starting_between(
      pos_1, pos_2 - PROM_SIZE + 1, extracted_promoters[Strand::LEADING]);

  extract_lagging_promoters_starting_between(
      pos_1 + PROM_SIZE - 1, pos_2, extracted_promoters[Strand::LAGGING]);

  return extracted_promoters;
}

void PromoterList::insert_promoters(PromoterList& promoters_to_insert) {
  for (auto strand: {Strand::LEADING, Strand::LAGGING}) {
    if (promoters_to_insert[strand].size() <= 0) {
      continue;
    }
    // Get to the right position in individual's list (first promoter after the inserted segment)
    int32_t from_pos = promoters_to_insert[strand].back().pos;

    auto pos = find_if(single_stranded_list(strand).begin(),
                       single_stranded_list(strand).end(),
                       [from_pos, strand](Promoter& r) {
                         if (strand == Strand::LEADING) {
                           return r.pos >= from_pos;
                         }
                         else {
                           return r.pos < from_pos;
                         }
                       });

    // Insert the promoters in the individual's RNA list
    for (auto& to_insert : promoters_to_insert[strand]) {
      // TODO vld: could be compacted in a unique emplace(pos, to_insert) ?
      to_insert.to_compute_end_ = true;
      to_insert.rna = nullptr;

      if (pos != single_stranded_list(strand).end()) {
        single_stranded_list(strand).insert(pos, to_insert);
      }
      else {
        single_stranded_list(strand).push_back(to_insert);
      }
    }
  }
}

void PromoterList::insert_promoters_at(PromoterList& promoters_to_insert,
                                       int32_t pos,
                                       int32_t dna_len) {
  for (auto strand: {Strand::LEADING, Strand::LAGGING}) {
    if (promoters_to_insert[strand].size() <= 0) {
      continue;
    }
    // Get to the right position in individual's list (first promoter after the inserted segment)
    auto first = find_if(single_stranded_list(strand).begin(),
                         single_stranded_list(strand).end(), [pos, strand](Promoter& r) {
          if (strand == Strand::LEADING) {
            return r.pos >= pos;
          } else {
            return r.pos < pos;
          }
        });

    // Insert the promoters in the individual's RNA list
    for (auto& to_insert: promoters_to_insert[strand]) {
      // Update promoter position
      to_insert.pos             = Utils::mod(to_insert.pos + pos, dna_len);  //shift_position(pos, dna_->length());
      to_insert.to_compute_end_ = true;
      to_insert.rna             = nullptr;
      // Insert
      if (first != single_stranded_list(strand).end()) {
        single_stranded_list(strand).insert(first, to_insert);
      } else {
        single_stranded_list(strand).push_back(to_insert);
      }
    }
  }
}

void PromoterList::invert_promoters_included_in(int32_t pos1, int32_t pos2) {
  int32_t segment_length = pos2 - pos1;

  if (segment_length < PROM_SIZE) {
    return;
  }

  // 1) Extract the promoters completely included on the segment to be inverted
  PromoterList inverted_promoters = extract_promoters_included_in(pos1, pos2);

  // 2) Invert segment's promoters
  inverted_promoters.invert_promoters(pos1, pos2);

  // 3) Reinsert the inverted promoters
  insert_promoters(inverted_promoters);
}

void PromoterList::shift_promoters(int32_t delta_pos,
                                   int32_t seq_length) {
  for (auto& strand: {Strand::LEADING, Strand::LAGGING})
    for (auto& rna: single_stranded_list(strand)) {
      rna.pos             = Utils::mod(rna.pos + delta_pos, seq_length);
      rna.to_compute_end_ = true;
      rna.rna             = nullptr;
    }
}

void PromoterList::invert_promoters(int32_t pos1, int32_t pos2) {
  // Exchange LEADING and LAGGING lists
  single_stranded_list(Strand::LEADING).swap(single_stranded_list(Strand::LAGGING));
  // Update the position and strand of each promoter to be inverted...
  for (auto strand: {Strand::LEADING, Strand::LAGGING})
    for (auto& rna : single_stranded_list(strand)) {
      rna.pos = pos1 + pos2 - rna.pos - 1;
      rna.strand_ = strand;
      rna.to_compute_end_ = true;
      rna.rna = nullptr;
    }
}

void PromoterList::remove_leading_promoters_starting_between(int32_t pos_1, int32_t pos_2) {
  if (pos_1 > pos_2) {
    remove_leading_promoters_starting_after(pos_1);
    remove_leading_promoters_starting_before(pos_2);
  } else {
    auto& strand = single_stranded_list(Strand::LEADING);

    // Delete RNAs until we pass pos_2 (or we reach the end of the list)
    // STL Warning: don't erase the current iterator in the for-loop!
    auto init_loop = find_if(strand.begin(), strand.end(), [pos_1](Promoter& r) { return r.pos >= pos_1; });

    auto range_end = init_loop;

    // if (indiv_->indiv_id == 290 && AeTime::time() == 3)
    //     printf("Delete range (after %d) %d (cur %d) :: %d \n",pos_1,(*init_loop).pos,(*next(init_loop)).pos,
    //                     init_loop == strand.end());

    for (auto it = init_loop, nextit = it; it != strand.end() and it->pos < pos_2; it = nextit) {
      nextit = next(it);
      strand.erase(it);
      range_end = nextit;
      // if (indiv_->indiv_id == 290 && AeTime::time() == 3)
      //     printf("Update range end %d (cur %d)\n",(*range_end).pos,(*it).pos);
    }
  }
}

void PromoterList::remove_leading_promoters_starting_after(int32_t pos) {
  auto& strand = single_stranded_list(Strand::LEADING);

  auto init_loop = find_if(strand.begin(), strand.end(),
                           [pos](Promoter& r) { return r.pos >= pos; });
  auto init_loop2 = init_loop;
  if (init_loop2 != strand.begin())
    init_loop2 = prev(init_loop2);
  for (auto it = init_loop,
            nextit = it;
       it != strand.end();
       it = nextit) {
    nextit = next(it);
    strand.erase(it);
  }
}

void PromoterList::remove_leading_promoters_starting_before(int32_t pos) {
  auto& strand = single_stranded_list(Strand::LEADING);
  // Delete RNAs until we reach pos (or we reach the end of the list)
  auto range_end = strand.begin();
  for (auto it = strand.begin(),
            nextit = it;
       it != strand.end() and it->pos < pos;
       it = nextit) {
    nextit = next(it);
    strand.erase(it);
    range_end = nextit;
  }
}

void PromoterList::remove_lagging_promoters_starting_between(int32_t pos_1, int32_t pos_2, int32_t dna_len) {
  if (pos_1 == dna_len) pos_1 = 0;
  if (pos_2 == 0) pos_2 = dna_len;
  if (pos_1 >
      pos_2) { // vld: that's a weird case... really do this? used from remove_promoters_around()
    remove_lagging_promoters_starting_after(pos_1);
    remove_lagging_promoters_starting_before(pos_2);
  }
  else {
    auto& strand = single_stranded_list(Strand::LAGGING);
    // Delete RNAs until we pass pos_1 (or we reach the end of the list)
    auto init_loop = find_if(strand.begin(),
                             strand.end(),
                             [pos_2](Promoter& r) {
                               return r.pos < pos_2;
                             });
    auto range_end = strand.begin();

    for (auto it = init_loop,
              nextit = it;
         it != strand.end() and it->pos >= pos_1;
         it = nextit) {
      nextit = next(it);
      strand.erase(it);
      range_end = nextit;
    }
  }
}

void PromoterList::remove_lagging_promoters_starting_after(int32_t pos) {
  auto& strand = single_stranded_list(Strand::LAGGING);
  // Delete RNAs until we pass pos (or we reach the end of the list)
  auto range_end = strand.begin();
  for (auto it = strand.begin(),
            nextit = it;
       it != strand.end() and it->pos >= pos;
       it = nextit) {
    nextit = next(it);
    strand.erase(it);
    range_end = nextit;
  }
}

void PromoterList::remove_lagging_promoters_starting_before(int32_t pos) {
  auto& strand = single_stranded_list(Strand::LAGGING);
  // Delete RNAs until we reach pos (or we reach the end of the list)

  auto init_loop = find_if(strand.begin(),
                           strand.end(),
                           [pos](Promoter& r) { return r.pos < pos; });
  for (auto it = init_loop,
            nextit = it;
       it != strand.end();
       it = nextit) {
    nextit = next(it);
    strand.erase(it);
  }
}

void PromoterList::move_all_leading_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len) {
  auto& strand   = single_stranded_list(Strand::LEADING);
  auto init_loop = find_if(strand.begin(), strand.end(), [pos](Promoter& r) { return r.pos >= pos; });

  for (auto rna = init_loop; rna != strand.end(); ++rna) {
    (*rna).pos = Utils::mod((*rna).pos + delta_pos, dna_len);

    (*rna).to_compute_end_ = true;
    if ((*rna).rna != nullptr) {
      (*rna).rna->is_init_   = false;
      (*rna).rna->is_coding_ = false;
    }
  }
}

void PromoterList::move_all_lagging_promoters_after(int32_t pos, int32_t delta_pos, int32_t dna_len) {
  auto& strand = single_stranded_list(Strand::LAGGING);
  // Update RNAs until we pass pos (or we reach the end of the list)
  auto start_loop = strand.begin();
  for (auto rna = strand.begin(); rna != strand.end() and rna->pos >= pos; ++rna) {
    (*rna).pos             = Utils::mod((*rna).pos + delta_pos, dna_len);
    (*rna).to_compute_end_ = true;
    if ((*rna).rna != nullptr) {
      (*rna).rna->is_init_   = false;
      (*rna).rna->is_coding_ = false;
    }
    start_loop = rna;
  }
}

void PromoterList::look_for_new_leading_promoters_starting_between(const Dna_7& dna, int32_t pos_1, int32_t pos_2) {
  if (pos_1 >= pos_2) {
    look_for_new_leading_promoters_starting_after(dna, pos_1);
    look_for_new_leading_promoters_starting_before(dna, pos_2);
    return;
  }
  int8_t dist; // Hamming distance of the sequence from the promoter consensus

  for (int32_t i = pos_1; i < pos_2; i++) {
    dist = List_Metadata::is_promoter_leading(dna, i);
    if (dist <= PROM_MAX_DIFF) // dist takes the hamming distance of the sequence from the consensus
    {

      // Look for the right place to insert the new promoter in the list
      auto& strand = single_stranded_list(Strand::LEADING);

      auto first = find_if(strand.begin(),
                           strand.end(),
                           [i](Promoter& r) { return r.pos >= i; });


      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LEADING).emplace(first, i, dist, Strand::LEADING);
      }
    }
  }
}

void PromoterList::look_for_new_leading_promoters_starting_after(const Dna_7& dna, int32_t pos) {
  // Hamming distance of the sequence from the promoter consensus
  int8_t dist;

  // rna list node used to find the new promoter's place in the list
  for (int32_t i = pos; i < dna.length(); i++) {
    dist = List_Metadata::is_promoter_leading(dna, i);
    if (dist <= PROM_MAX_DIFF) { // dist takes the hamming distance of the sequence from the consensus
      // Look for the right place to insert the new promoter in the list
      auto& strand = single_stranded_list(Strand::LEADING);
      auto first = find_if(strand.begin(),
                           strand.end(),
                           [i](Promoter& r) {
                             return r.pos >= i;
                           });

      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LEADING).emplace(first, i, dist, Strand::LEADING);
      }
    }
  }
}

void PromoterList::look_for_new_leading_promoters_starting_before(const Dna_7& dna, int32_t pos) {
  // Hamming distance of the sequence from the promoter consensus
  int8_t dist;

  auto& strand = single_stranded_list(Strand::LEADING);
  auto first = strand.begin(); // TODO vld: should it not be reset at each loop step?

  for (int32_t i = 0; i < pos; i++) {
    dist = List_Metadata::is_promoter_leading(dna, i);
    if (dist <= PROM_MAX_DIFF) {
      // Look for the right place to insert the new promoter in the list

      first = find_if(first,
                      strand.end(),
                      [i](Promoter& r) { return r.pos >= i; });

      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LEADING).emplace(first, i, dist, Strand::LEADING);

      }
    }
  }
}

void PromoterList::look_for_new_lagging_promoters_starting_between(const Dna_7& dna, int32_t pos_1, int32_t pos_2) {
  if (pos_1 >= pos_2) {
    look_for_new_lagging_promoters_starting_after(dna, pos_1);
    look_for_new_lagging_promoters_starting_before(dna, pos_2);
    return;
  }

  int8_t dist; // Hamming distance of the sequence from the promoter consensus
  for (int32_t i = pos_2 - 1; i >= pos_1; i--) {
    dist = List_Metadata::is_promoter_lagging(dna, i);
    if (dist <= PROM_MAX_DIFF) {
      // Look for the right place to insert the new promoter in the list
      auto& strand = single_stranded_list(Strand::LAGGING);

      auto first = find_if(strand.begin(),
                           strand.end(),
                           [i](Promoter& r) { return r.pos <= i; });

      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LAGGING).emplace(first, i, dist, Strand::LAGGING);
      }
    }
  }
}

void PromoterList::look_for_new_lagging_promoters_starting_after(const Dna_7& dna, int32_t pos) {
  // Hamming distance of the sequence from the promoter consensus
  int8_t dist;
  auto& strand = single_stranded_list(Strand::LAGGING);
  auto first = strand.begin();

  for (int32_t i = dna.length() - 1; i >= pos; i--) {
    dist = List_Metadata::is_promoter_lagging(dna, i);
    if (dist <= PROM_MAX_DIFF) {
      // Look for the right place to insert the new promoter in the list
      first = find_if(first,
                      strand.end(),
                      [i](Promoter& r) { return r.pos <= i; });

      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LAGGING).emplace(first, i, dist, Strand::LAGGING);
      }
    }
  }
}

void PromoterList::look_for_new_lagging_promoters_starting_before(const Dna_7& dna, int32_t pos) {
  int8_t dist;

  // rna list node used to find the new promoter's place in the list
  auto& strand = single_stranded_list(Strand::LAGGING);
  auto first = strand.begin();

  for (int32_t i = pos - 1; i >= 0; i--) {
    dist = List_Metadata::is_promoter_lagging(dna, i);
    if (dist <= PROM_MAX_DIFF) {
      assert (i >= 0 && i < dna.length());
      // Look for the right place to insert the new promoter in the list
      first = find_if(first,
                      strand.end(),
                      [i](Promoter& r) {
                        return r.pos <= i;
                      });

      if (first == strand.end() or first->pos != i) {
        single_stranded_list(Strand::LAGGING).emplace(first, i, dist, Strand::LAGGING);
      }
    }
  }
}

PromoterList PromoterList::promoters_included_in(int32_t pos_1,
                                         int32_t pos_2,
                                         int32_t dna_len) {
  PromoterList promoter_list;

  if (pos_1 < pos_2) {
    int32_t seg_length = pos_2 - pos_1;

    if (seg_length >= PROM_SIZE) {
      lst_promoters(Strand::LEADING, BETWEEN, pos_1, pos_2 - PROM_SIZE + 1, promoter_list[Strand::LEADING]);
      lst_promoters(Strand::LAGGING, BETWEEN, pos_2, pos_1 + PROM_SIZE - 1, promoter_list[Strand::LAGGING]);
    }
  } else {
    int32_t seg_length = dna_len + pos_2 - pos_1;

    if (seg_length >= PROM_SIZE) {
      bool is_near_end_of_genome       = (pos_1 + PROM_SIZE > dna_len);
      bool is_near_beginning_of_genome = (pos_2 - PROM_SIZE < 0);

      if (!is_near_end_of_genome && !is_near_beginning_of_genome) {
        lst_promoters(Strand::LEADING, AFTER, pos_1, -1, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LEADING, BEFORE, -1, pos_2 - PROM_SIZE + 1, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LAGGING, AFTER, pos_2, -1, promoter_list[Strand::LAGGING]);
        lst_promoters(Strand::LAGGING, BEFORE, -1, pos_1 + PROM_SIZE - 1, promoter_list[Strand::LAGGING]);
      } else if (!is_near_end_of_genome)  // => && is_near_beginning_of_genome
      {
        lst_promoters(Strand::LEADING, BETWEEN, pos_1, pos_2 - PROM_SIZE + 1 + dna_len, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LAGGING, AFTER, pos_2, -1, promoter_list[Strand::LAGGING]);
        lst_promoters(Strand::LAGGING, BEFORE, -1, pos_1 + PROM_SIZE - 1, promoter_list[Strand::LAGGING]);
      } else if (!is_near_beginning_of_genome)  // => && is_near_end_of_genome
      {
        lst_promoters(Strand::LEADING, AFTER, pos_1, -1, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LEADING, BEFORE, -1, pos_2 - PROM_SIZE + 1, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LAGGING, BETWEEN, pos_2, pos_1 + PROM_SIZE - 1 - dna_len, promoter_list[Strand::LAGGING]);
      } else  // is_near_end_of_genome && is_near_beginning_of_genome
      {
        lst_promoters(Strand::LEADING, BETWEEN, pos_1, pos_2 - PROM_SIZE + 1 + dna_len, promoter_list[Strand::LEADING]);
        lst_promoters(Strand::LAGGING, BETWEEN, pos_2, pos_1 + PROM_SIZE - 1 - dna_len, promoter_list[Strand::LAGGING]);
      }
    }
  }

  return promoter_list;
}

void PromoterList::extract_leading_promoters_starting_between(
    int32_t pos_1, int32_t pos_2, SingleStranded& extracted_promoters) {
  // Find the first promoters in the interval
  auto& strand = single_stranded_list(Strand::LEADING);

  auto first = find_if(strand.begin(), strand.end(), [pos_1](Promoter& p) { return p.pos >= pos_1; });

  if (first == strand.end() or first->pos >= pos_2) {
    return;
  }

  // Find the last promoters in the interval

  auto end = find_if(first, strand.end(), [pos_2](Promoter& p) { return p.pos >= pos_2; });

  // Extract the promoters (remove them from the individual's list and put them in extracted_promoters)
  SingleStranded promoters_1D;
  for (auto it = first; it != end; it++) {
    Promoter prom(*it);
    prom.to_compute_end_ = true;
    prom.rna             = nullptr;
    promoters_1D.push_back(std::move(prom));
  }

  extracted_promoters.insert(extracted_promoters.end(), promoters_1D.begin(), promoters_1D.end());

  strand.erase(first, end);
}

void PromoterList::extract_lagging_promoters_starting_between(
    int32_t pos_1, int32_t pos_2, SingleStranded& extracted_promoters) {
  // Find the first promoters in the interval (if any)
  auto& strand = single_stranded_list(Strand::LAGGING);

  auto first = find_if(strand.begin(), strand.end(), [pos_2](Promoter& r) { return r.pos < pos_2; });

  if (first == strand.end() or first->pos < pos_1) {
    return;
  }

  // Find the last promoters in the interval
  auto end = find_if(first, strand.end(), [pos_1](Promoter& r) { return r.pos < pos_1; });

  // Extract the promoters (remove them from the individual's list and put the in extracted_promoters)
  SingleStranded promoters_1D;
  for (auto it = first; it != end; it++) {
    Promoter prom(*it);
    prom.to_compute_end_ = true;
    prom.rna             = nullptr;
    promoters_1D.push_back(prom);
  }

  extracted_promoters.insert(extracted_promoters.end(), promoters_1D.begin(), promoters_1D.end());
  strand.erase(first, end);
}

} // namespace aevol
