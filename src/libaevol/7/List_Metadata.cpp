// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#include "List_Metadata.h"

#include "Rna_7.h"
#include "AeTime.h"

#include <algorithm>
#include <iterator>
#include <list>

#include "Individual_7.h"

namespace aevol {

#ifdef __REGUL
List_Metadata::List_Metadata(Individual_7* indiv, List_Metadata* metadata) {
#else
List_Metadata::List_Metadata(Individual_7* indiv, List_Metadata*) {
#endif
  indiv_ = indiv;

#ifdef __REGUL
  if (exp_manager->exp_s()->get_with_heredity()) {
    inherited_proteins_.clear();

    for (auto prot : metadata->proteins_) {
      if (prot!=nullptr && prot->is_init_ && (!prot->signal_ && !prot->inherited_))
        if (prot->e >
            exp_manager->exp_s()->get_protein_presence_limit() && !(prot->signal_)) {

          Protein_7* inherited_prot = new Protein_7(prot, exp_manager->exp_s()->get_with_heredity());
          inherited_prot->inherited_ = true;
          inherited_proteins_.push_back(inherited_prot);
        }
    }
  }
#endif
  // TODO: Add inherited protein if REGUL

  set_iterators();
}

List_Metadata::List_Metadata(Individual_7* indiv) {
  indiv_ = indiv;

  set_iterators();
}

List_Metadata::~List_Metadata() {
  for (auto& rna : rnas_) {
    delete rna;
  }
  rnas_.clear();

  for (auto& protein : proteins_) {
    delete protein;
  }

#ifdef __REGUL
  signal_proteins_.clear();

  if (exp_manager->exp_s()->get_with_heredity()) {
    for (auto& protein : inherited_proteins_) {
      delete protein;
    }
  }
#endif

  terminator_lead_.clear();
  terminator_lag_.clear();
}

int8_t List_Metadata::is_promoter_leading(const Dna_7& dna, int pos) {
#ifdef BASE_2
  #ifdef VANILLA_SEARCH
  int8_t prom_dist_leading[26];
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist_leading[motif_id] =
        PROM_SEQ_LEAD[motif_id] ==
                dna.data_[pos + motif_id >= len ? pos + motif_id - len : pos + motif_id]
            ? 0 : 1;
  }

  return prom_dist_leading[0] +
         prom_dist_leading[1] +
         prom_dist_leading[2] +
         prom_dist_leading[3] +
         prom_dist_leading[4] +
         prom_dist_leading[5] +
         prom_dist_leading[6] +
         prom_dist_leading[7] +
         prom_dist_leading[8] +
         prom_dist_leading[9] +
         prom_dist_leading[10] +
         prom_dist_leading[11] +
         prom_dist_leading[12] +
         prom_dist_leading[13] +
         prom_dist_leading[14] +
         prom_dist_leading[15] +
         prom_dist_leading[16] +
         prom_dist_leading[17] +
         prom_dist_leading[18] +
         prom_dist_leading[19] +
         prom_dist_leading[20] +
         prom_dist_leading[21];
  #elif CMOD_SEARCH
  int8_t prom_dist_leading[26];
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist_leading[motif_id] =
        PROM_SEQ_LEAD[motif_id] ==
                dna.data_[Utils::mod(pos + motif_id, len)]
            ? 0 : 1;
  }

  return prom_dist_leading[0] +
         prom_dist_leading[1] +
         prom_dist_leading[2] +
         prom_dist_leading[3] +
         prom_dist_leading[4] +
         prom_dist_leading[5] +
         prom_dist_leading[6] +
         prom_dist_leading[7] +
         prom_dist_leading[8] +
         prom_dist_leading[9] +
         prom_dist_leading[10] +
         prom_dist_leading[11] +
         prom_dist_leading[12] +
         prom_dist_leading[13] +
         prom_dist_leading[14] +
         prom_dist_leading[15] +
         prom_dist_leading[16] +
         prom_dist_leading[17] +
         prom_dist_leading[18] +
         prom_dist_leading[19] +
         prom_dist_leading[20] +
         prom_dist_leading[21];

  #elif AMOD_SEARCH
  int8_t prom_dist_leading[26];
  int len = dna.length();

  if (pos+22 >= len) {
    for (int motif_id = 0; motif_id < 22; motif_id++) {
      prom_dist_leading[motif_id] =
          PROM_SEQ_LEAD[motif_id] ==
                  dna.data_[Utils::mod(pos + motif_id, len)]
              ? 0 : 1;
    }
  } else {
    for (int motif_id = 0; motif_id < 22; motif_id++) {
      prom_dist_leading[motif_id] =
          PROM_SEQ_LEAD[motif_id] ==
                  dna.data_[pos + motif_id]
              ? 0 : 1;
    }
  }

  return prom_dist_leading[0] +
         prom_dist_leading[1] +
         prom_dist_leading[2] +
         prom_dist_leading[3] +
         prom_dist_leading[4] +
         prom_dist_leading[5] +
         prom_dist_leading[6] +
         prom_dist_leading[7] +
         prom_dist_leading[8] +
         prom_dist_leading[9] +
         prom_dist_leading[10] +
         prom_dist_leading[11] +
         prom_dist_leading[12] +
         prom_dist_leading[13] +
         prom_dist_leading[14] +
         prom_dist_leading[15] +
         prom_dist_leading[16] +
         prom_dist_leading[17] +
         prom_dist_leading[18] +
         prom_dist_leading[19] +
         prom_dist_leading[20] +
         prom_dist_leading[21];

  #elif B1MOD_SEARCH
  int8_t prom_dist_leading[26];
  int32_t len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist_leading[motif_id] =
        PROM_SEQ_LEAD[motif_id] ==
                dna.data_[(pos + motif_id) - ((pos + motif_id) >= len) * len]
            ? 0 : 1;
  }

  return prom_dist_leading[0] +
         prom_dist_leading[1] +
         prom_dist_leading[2] +
         prom_dist_leading[3] +
         prom_dist_leading[4] +
         prom_dist_leading[5] +
         prom_dist_leading[6] +
         prom_dist_leading[7] +
         prom_dist_leading[8] +
         prom_dist_leading[9] +
         prom_dist_leading[10] +
         prom_dist_leading[11] +
         prom_dist_leading[12] +
         prom_dist_leading[13] +
         prom_dist_leading[14] +
         prom_dist_leading[15] +
         prom_dist_leading[16] +
         prom_dist_leading[17] +
         prom_dist_leading[18] +
         prom_dist_leading[19] +
         prom_dist_leading[20] +
         prom_dist_leading[21];
  #elif B2MOD_SEARCH
  int8_t prom_dist_leading[26];
  int32_t len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist_leading[motif_id] =
        PROM_SEQ_LEAD[motif_id] ==
                dna.data_[(pos + motif_id) + ((((uint32_t)((pos + motif_id) - len)) >> 31) -1 )* len]
            ? 0 : 1;
  }

  return prom_dist_leading[0] +
         prom_dist_leading[1] +
         prom_dist_leading[2] +
         prom_dist_leading[3] +
         prom_dist_leading[4] +
         prom_dist_leading[5] +
         prom_dist_leading[6] +
         prom_dist_leading[7] +
         prom_dist_leading[8] +
         prom_dist_leading[9] +
         prom_dist_leading[10] +
         prom_dist_leading[11] +
         prom_dist_leading[12] +
         prom_dist_leading[13] +
         prom_dist_leading[14] +
         prom_dist_leading[15] +
         prom_dist_leading[16] +
         prom_dist_leading[17] +
         prom_dist_leading[18] +
         prom_dist_leading[19] +
         prom_dist_leading[20] +
         prom_dist_leading[21];
  #else // OLD_SEARCH
  int8_t prom_dist_leading = 0;
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist_leading +=
        PROM_SEQ_LEAD[motif_id] ==
                dna.data_[pos + motif_id >= len ? pos + motif_id - len : pos + motif_id]
            ? 0 : 1;
    if (prom_dist_leading>PROM_MAX_DIFF)
      break;
  }

  return prom_dist_leading;
  #endif
#elif BASE_4
  int8_t prom_dist_leading = 0;
  int len = dna.length();

  for (int motif_id = 0; motif_id < PROM_SIZE; motif_id++) {
    prom_dist_leading +=
        PROM_SEQ[motif_id] != dna.data_[
                                  pos + motif_id >= len ?
                                                        pos - len + motif_id : pos + motif_id
    ];

    if (prom_dist_leading > PROM_MAX_DIFF)
      break;
  }

  return prom_dist_leading;
#endif
}

int8_t List_Metadata::is_promoter_lagging(const Dna_7& dna, int pos) {
#ifdef BASE_2
  #ifdef VANILLA_SEARCH
  int8_t prom_dist[26];
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist[motif_id] =
        PROM_SEQ_LAG[motif_id] ==
                dna.data_[pos - motif_id < 0 ? len + pos - motif_id : pos - motif_id]
            ? 0 : 1;
  }

  return prom_dist[0] +
         prom_dist[1] +
         prom_dist[2] +
         prom_dist[3] +
         prom_dist[4] +
         prom_dist[5] +
         prom_dist[6] +
         prom_dist[7] +
         prom_dist[8] +
         prom_dist[9] +
         prom_dist[10] +
         prom_dist[11] +
         prom_dist[12] +
         prom_dist[13] +
         prom_dist[14] +
         prom_dist[15] +
         prom_dist[16] +
         prom_dist[17] +
         prom_dist[18] +
         prom_dist[19] +
         prom_dist[20] +
         prom_dist[21];
  #elif CMOD_SEARCH
  int8_t prom_dist[26];
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist[motif_id] =
        PROM_SEQ_LAG[motif_id] ==
                dna.data_[Utils::mod(pos - motif_id,len)]
            ? 0 : 1;
  }

  return prom_dist[0] +
         prom_dist[1] +
         prom_dist[2] +
         prom_dist[3] +
         prom_dist[4] +
         prom_dist[5] +
         prom_dist[6] +
         prom_dist[7] +
         prom_dist[8] +
         prom_dist[9] +
         prom_dist[10] +
         prom_dist[11] +
         prom_dist[12] +
         prom_dist[13] +
         prom_dist[14] +
         prom_dist[15] +
         prom_dist[16] +
         prom_dist[17] +
         prom_dist[18] +
         prom_dist[19] +
         prom_dist[20] +
         prom_dist[21];
  #elif AMOD_SEARCH
  int8_t prom_dist[26];
  int len = dna.length();

  if (pos-22 < 0) {

    for (int motif_id = 0; motif_id < 22; motif_id++) {
      prom_dist[motif_id] =
          PROM_SEQ_LAG[motif_id] ==
                  dna.data_[Utils::mod(pos - motif_id,len)]
              ? 0 : 1;
    }
  } else {
    for (int motif_id = 0; motif_id < 22; motif_id++) {
      prom_dist[motif_id] =
          PROM_SEQ_LAG[motif_id] ==
                  dna.data_[pos - motif_id]
              ? 0 : 1;
    }
  }

  return prom_dist[0] +
         prom_dist[1] +
         prom_dist[2] +
         prom_dist[3] +
         prom_dist[4] +
         prom_dist[5] +
         prom_dist[6] +
         prom_dist[7] +
         prom_dist[8] +
         prom_dist[9] +
         prom_dist[10] +
         prom_dist[11] +
         prom_dist[12] +
         prom_dist[13] +
         prom_dist[14] +
         prom_dist[15] +
         prom_dist[16] +
         prom_dist[17] +
         prom_dist[18] +
         prom_dist[19] +
         prom_dist[20] +
         prom_dist[21];
  #elif B1MOD
  int8_t prom_dist[26];
  int32_t len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist[motif_id] =
        PROM_SEQ_LAG[motif_id] ==
                dna.data_[(pos - motif_id) + ((pos - motif_id) < 0) * len]
            ? 0 : 1;
  }

  return prom_dist[0] +
         prom_dist[1] +
         prom_dist[2] +
         prom_dist[3] +
         prom_dist[4] +
         prom_dist[5] +
         prom_dist[6] +
         prom_dist[7] +
         prom_dist[8] +
         prom_dist[9] +
         prom_dist[10] +
         prom_dist[11] +
         prom_dist[12] +
         prom_dist[13] +
         prom_dist[14] +
         prom_dist[15] +
         prom_dist[16] +
         prom_dist[17] +
         prom_dist[18] +
         prom_dist[19] +
         prom_dist[20] +
         prom_dist[21];

  #elif B2MOD
  int8_t prom_dist[26];
  int32_t len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist[motif_id] =
        PROM_SEQ_LAG[motif_id] ==
                dna.data_[ (pos - motif_id) + (((uint32_t)((pos - motif_id))) >> 31) * len]
            ? 0 : 1;
  }

  return prom_dist[0] +
         prom_dist[1] +
         prom_dist[2] +
         prom_dist[3] +
         prom_dist[4] +
         prom_dist[5] +
         prom_dist[6] +
         prom_dist[7] +
         prom_dist[8] +
         prom_dist[9] +
         prom_dist[10] +
         prom_dist[11] +
         prom_dist[12] +
         prom_dist[13] +
         prom_dist[14] +
         prom_dist[15] +
         prom_dist[16] +
         prom_dist[17] +
         prom_dist[18] +
         prom_dist[19] +
         prom_dist[20] +
         prom_dist[21];
  #else
  int8_t prom_dist = 0;
  int len = dna.length();

  for (int motif_id = 0; motif_id < 22; motif_id++) {
    prom_dist+=
        PROM_SEQ_LAG[motif_id] ==
                dna.data_[Utils::mod(pos - motif_id,len)]
            ? 0 : 1;
    if (prom_dist>PROM_MAX_DIFF)
      break;
  }

  return prom_dist;
  #endif
#elif BASE_4
  int8_t prom_dist = 0;
  int len = dna.length();

  for (int motif_id = 0; motif_id < PROM_SIZE; motif_id++) {
    prom_dist +=
        PROM_SEQ[motif_id]
        != get_complementary_base(dna.data_[  // this is the same as comparing the two opposites (comparing opposite PROM with opposite strand)
               pos - motif_id < 0 ?
                                  pos + len - motif_id : pos - motif_id
    ]);
    if (prom_dist > PROM_MAX_DIFF)
      break;
  }

  return prom_dist;
#endif
}

int8_t List_Metadata::is_shine_dal_start_prot_leading(int pos) {
#ifdef BASE_2
  int32_t len = indiv_->dna_->length();
  int8_t start[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  for (int k = 0; k < 9; k++) {
    int k_t = k >= 6 ? k + 4 : k;

    if (indiv_->dna_->data_[(pos + k_t) + ((((uint32_t)((pos + k_t) - len)) >> 31) -1 )* len] ==
        SHINE_DAL_SEQ_LEAD_7[k]) {
      start[k] = true;
    } else {
      start[k] = false;
      break;
    }
  }

  return start[0] + start[1] + start[2] + start[3] + start[4] + start[5] + start[6]
         + start[7] + start[8];
#elif BASE_4
  for (int k = 0; k < SHINE_DAL_SIZE; k++) {
    if (indiv_->dna_->get_lead(pos + k) != SHINE_DAL_SEQ[k]) {
      // printf("FAILED Search SHine Dal at %d => %d\n",pos,k);
      return false;
    }
  }

  int codon_pos = pos + SHINE_DAL_SIZE + SHINE_START_SPACER;

  // if (indiv_->indiv_id == 884) printf("%d -- %d -- Shine dal found %d, start %d : %c %c %c SHine %c %c %c %c %c %c\n",
  //         AeTime::time(),
  //         indiv_->indiv_id,pos,bases_to_codon_value(
  //         indiv_->dna_->get_lead(codon_pos    ),
  //         indiv_->dna_->get_lead(codon_pos + 1),
  //         indiv_->dna_->get_lead(codon_pos + 2)),
  //         indiv_->dna_->get_lead(codon_pos    ),
  //         indiv_->dna_->get_lead(codon_pos + 1),
  //         indiv_->dna_->get_lead(codon_pos + 2),
  //         indiv_->dna_->get_lead(pos    ),
  //         indiv_->dna_->get_lead(pos + 1),
  //         indiv_->dna_->get_lead(pos + 2),
  //         indiv_->dna_->get_lead(pos + 3),
  //         indiv_->dna_->get_lead(pos + 4),
  //         indiv_->dna_->get_lead(pos + 5));


  if(!is_start_codon(bases_to_codon_value(
          indiv_->dna_->get_lead(codon_pos    ),
          indiv_->dna_->get_lead(codon_pos + 1),
          indiv_->dna_->get_lead(codon_pos + 2)
              ))) {
    return false;
  }

  return true;
#endif
}

int8_t List_Metadata::is_shine_dal_start_prot_lagging(int pos) {
#ifdef BASE_2
  int32_t len = indiv_->dna_->length();
  int8_t start = false;

  for (int k = 0; k < 9; k++) {
    int k_t = k >= 6 ? k + 4 : k;

    if (indiv_->dna_->data_[(pos - k_t) + (((uint32_t)((pos - k_t))) >> 31) * len] ==
        SHINE_DAL_SEQ_LAG_7[k]) {
      start = true;
    } else {
      start = false;
      break;
    }
  }
  return start;
#elif BASE_4
  for (int k = 0; k < SHINE_DAL_SIZE; k++) {
    // call to lead instead of lag to simplify computation
    // => (complementary(a) != complementary(b) <=> a != b)
    //   if (pos == 2002) printf("%d :: %d --> %c != %c\n",pos-k,k,indiv_->dna_->get_lead(pos - k),SHINE_DAL_SEQ_LAG[k]);

    if (indiv_->dna_->get_lag(pos - k) != SHINE_DAL_SEQ[k]) {
      return false;
    }
  }

  int codon_pos = pos - SHINE_DAL_SIZE - SHINE_START_SPACER;

  // if (indiv_->indiv_id == 884) printf("%d -- %d -- Shine dal found %d, start %d : %c %c %c   %c %c %c %c %c %c\n",AeTime::time(),
  //         indiv_->indiv_id,pos,bases_to_codon_value(
  //         indiv_->dna_->get_lag(codon_pos    ),
  //         indiv_->dna_->get_lag(codon_pos - 1),
  //         indiv_->dna_->get_lag(codon_pos - 2)),
  //         indiv_->dna_->get_lag(codon_pos    ),
  //         indiv_->dna_->get_lag(codon_pos - 1),
  //         indiv_->dna_->get_lag(codon_pos - 2),
  //         indiv_->dna_->get_lag(pos    ),
  //         indiv_->dna_->get_lag(pos - 1),
  //         indiv_->dna_->get_lag(pos - 2),
  //         indiv_->dna_->get_lag(pos - 3),
  //         indiv_->dna_->get_lag(pos - 4),
  //         indiv_->dna_->get_lag(pos - 5));

  if(!is_start_codon(bases_to_codon_value(
          indiv_->dna_->get_lag(codon_pos    ),
          indiv_->dna_->get_lag(codon_pos - 1),
          indiv_->dna_->get_lag(codon_pos - 2)
              ))) {
    return false;
  }

  return true;
#endif
}

#ifdef BASE_4
bool List_Metadata::is_terminator_leading(int pos) {
  int32_t len = indiv_->dna_->length();

  // Palindrome
  for(auto i = 0; i < TERM_STEM_SIZE; i++) {
    if (!is_complementary_base(indiv_->dna_->data_[Utils::mod(pos + i,len)],
                               indiv_->dna_->data_[Utils::mod(pos + (TERM_SIZE-1) - i,len)])) {
      return false;
    }
  }

  // Poly-A sequence
  for (auto i = 0;
       i < exp_manager->exp_s()->terminator_polya_sequence_length();
       i++) {
    if (indiv_->dna_->data_[Utils::mod(pos + TERM_SIZE + i, len)] != BASE_A) {
      return false;
    }
  }

  return true;
}

bool List_Metadata::is_terminator_lagging(int pos) {
  int32_t len = indiv_->dna_->length();

  // Palindrome
  if (pos - TERM_SIZE -1 >= 0) {
    for(auto i = 0; i < TERM_STEM_SIZE; i++)
    {
      if (!is_complementary_base(indiv_->dna_->data_[pos - i], indiv_->dna_->data_[pos - (TERM_SIZE-1) + i])) {
        return false;
      }
    }
  } else {
    for(auto i = 0; i < TERM_STEM_SIZE; i++)
    {
      if (!is_complementary_base(indiv_->dna_->data_[Utils::mod(pos - i,len)], indiv_->dna_->data_[Utils::mod(pos - (TERM_SIZE-1) + i,len)])) {
        return false;
      }
    }
  }


  // Poly-A sequence
  for (auto i = 0;
       i < exp_manager->exp_s()->terminator_polya_sequence_length();
       i++) {
    if (indiv_->dna_->data_[Utils::mod(pos - TERM_SIZE - i, len)] != BASE_A) {
      return false;
    }
  }

  return true;
}
#endif

void List_Metadata::clean_remote() {
  terminator_lead_.clear();
  terminator_lag_.clear();

  for (auto& protein : proteins_) {
    delete protein;
  }

  proteins_.clear();

#ifdef __REGUL
  for (auto& protein : inherited_proteins_) {
    delete protein;
  }

  inherited_proteins_.clear();

  signal_proteins_.clear();
#endif

  for (auto& rna : rnas_) {
    delete rna;
  }

  rnas_.clear();

  set_iterators();
}

void List_Metadata::set_iterators() {
  it_rna_ = rnas_.begin();
  it_protein_ = proteins_.begin();
}

    int32_t List_Metadata::length() {
      return indiv_->dna_->length();
    }

#ifdef __REGUL
    void List_Metadata::reinit_proteins_inherited() {
      if (exp_manager->exp_s()->get_with_heredity()) {
        for (std::list<Protein_7*>::iterator it_protein = inherited_proteins_.begin(); it_protein != inherited_proteins_.end(); it_protein++) {
          delete (*(it_protein));
        }
        inherited_proteins_.clear();

        for (auto prot : proteins_) {
          if (prot!=nullptr && prot->is_init_ && prot->translated_)
            if (prot->e > exp_manager->exp_s()->get_protein_presence_limit()) {
              Protein_7* inherited_prot = new Protein_7(prot, exp_manager->exp_s()->get_with_heredity());
              inherited_prot->inherited_ = true;
              inherited_proteins_.push_back(inherited_prot);
            }
        }

        for (std::list<Protein_7*>::iterator it_protein = proteins_.begin(); it_protein != proteins_.end(); it_protein++) {
          if (!(*(it_protein))->translated_)
            delete (*(it_protein));
        }
        proteins_.clear();

        for (auto& prot : proteins_translated_) {
          if (prot!=nullptr && prot->is_init_) {
            for (auto&& rna: prot->rna_list_) {
              rna->affinity_list.clear();
            }
            proteins_.push_back(prot);
          }
        }

        rna_begin();
        for (int i = 0; i < rna_count(); i++) {
          Rna_7* rna = rna_next();
          if (rna != nullptr) {
            if (rna->is_coding_) {
              rna->affinity_list.clear();
            }
          }
        }
      }
    }
#endif


    Rna_7* List_Metadata::rnas(int idx) {
        auto it = rnas_.begin();
        std::advance(it, idx);
        return *(it);
    }

    void List_Metadata::rna_add(int, int32_t t_begin, int32_t t_end,
                 Strand strand, double t_e,
                 int32_t t_length,
                                Promoter* prom) {
        rnas_.emplace_back(new Rna_7(t_begin, t_end,
                 strand, t_e,
                 t_length,prom));
                 rna_count_add_++;
                 
                         it_rna_ = rnas_.begin();
    }


    void List_Metadata::rna_add(int, Rna_7* rna, Promoter* prom) {
                     rna->prom = prom;
                rnas_.push_back(rna);
        it_rna_ = rnas_.begin();
    }

    // void List_Metadata::rna_update(Rna_7* rna, PromoterStruct* prom, 
    //                  rna->init(prom, t_begin, t_end,t_leading_lagging, t_e, t_length);
    // }

    Rna_7* List_Metadata::rna_next() {
      Rna_7* rna = *(it_rna_);
        it_rna_++;
        cmp_rna++;
        return rna;
    }

    void List_Metadata::rna_begin() {
        it_rna_ = rnas_.begin();
    }

    bool List_Metadata::rna_end() {
        return it_rna_ == rnas_.end();
    }

    int List_Metadata::rna_count() {
        return rnas_.size();
    }



    void List_Metadata::rnas_clear() {
        rnas_.clear();
    }

    Protein_7* List_Metadata::proteins(int idx) {
        auto it = proteins_.begin();
        std::advance(it, idx);
        return *(it);
    }

    void List_Metadata::protein_add(int, Protein_7*prot) {
        proteins_.push_front(prot);
        #ifdef __REGUL
        if (prot->translated_)
            proteins_translated_.push_front(prot);
            #endif
    }

    void List_Metadata::protein_add(int32_t t_protein_start,
            int32_t t_protein_end,
            int32_t t_protein_length,
            Strand strand,
            double t_e, Rna_7* rna) {
          

        proteins_.emplace_back(new Protein_7(t_protein_start,t_protein_end,t_protein_length,strand,t_e,rna));

    }

    void List_Metadata::proteins_print(int step, int indiv_id) {
        int prot_idx=0;
        printf("Protein size = %zu\n", proteins_.size());

        for (auto prot : proteins_) {
            if (prot!=nullptr)
                if (prot->is_init_ 
                #ifdef __REGUL
                && !prot->signal_
                #endif
                )
                    printf("SIMD -- %d -- %d -- Protein %d : %.18e (Length %d)\n",step,indiv_id,prot->protein_start,prot->e,prot->protein_length);
            prot_idx++;
        }
    }

    Protein_7* List_Metadata::protein_next() {
      Protein_7* prot = *(it_protein_);
        it_protein_++;
        return prot;
    }

    void List_Metadata::protein_begin() {
        it_protein_ = proteins_.begin();
    }

    bool List_Metadata::protein_end() {
        return it_protein_ == proteins_.end();
    }

    int List_Metadata::proteins_count() {
        return proteins_.size();
    }

    void List_Metadata::set_proteins_count(int) {
        //protein_count_ = pcount;
    }

    void List_Metadata::proteins_resize(int) {
        //proteins_.resize(resize);
    }

    void List_Metadata::proteins_clear() {
        proteins_.clear();
    }

    void List_Metadata::proteins_remove_signal() {
        #ifdef __REGUL
            // proteins_.erase(proteins_.remove_if(proteins_.begin(),proteins_.end(), []() {return signal_; }),proteins_.end());
            for (std::list<Protein_7*>::iterator it_protein = proteins_.begin(); it_protein != proteins_.end(); ) {
                if ((*it_protein)->signal_) { 
                    Protein_7* to_delete = (*(it_protein));
                    it_protein = proteins_.erase(it_protein);
                    delete to_delete;
                } else {
                    ++it_protein;
                }
            }
            it_protein_ = proteins_.begin();

            signal_proteins_.clear();
        #endif
    }


#ifdef __REGUL
void List_Metadata::add_inherited_proteins() {
                    // if (indiv!=nullptr) printf("%d -- %d -- BEFORE -- Adding proteins inherited is %zu (%zu)\n",
                    //     AeTime::time(),indiv->indiv_id,
                    //     inherited_proteins_.size(), proteins_.size());
    if (! inherited_added_) {
        for (auto prot : inherited_proteins_) {
            if (! prot->signal_) {
                int glob_prot_idx = proteins_count();
                set_proteins_count(proteins_count() + 1);
                // printf("P_CPY_PTR %f %f %f %f :: %d %d %d\n",prot->h,prot->w,prot->m,prot->e,prot->is_init_,prot->signal_,prot->inherited_);
                protein_add(glob_prot_idx, new Protein_7(prot, exp_manager->exp_s()->get_with_heredity()));
                // printf("%d -- Indiv %d Add prot %d\n",AeTime::time(),indiv_->indiv_id,glob_prot_idx);
            }
        }
        inherited_added_ = true;
    }
    //  else
        inherited_added_ = true;
                    //   if (indiv!=nullptr) printf("%d -- %d -- AFTER -- Adding proteins inherited is %zu (%zu)\n",
                    //     AeTime::time(),indiv->indiv_id,
                    //     inherited_proteins_.size(), proteins_.size());
}
#endif
}
