// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FUZZYFACTORY_H
#define AEVOL_FUZZYFACTORY_H

#include "AbstractFuzzy.h"

namespace aevol {

enum class FuzzyFlavor : uint8_t {
  VECTOR = 1,
  DISCRETE_DOUBLE_TABLE = 3
};

// Tmp non-template implem of C++23 std::to_underlying
inline constexpr std::underlying_type_t<FuzzyFlavor> to_underlying(FuzzyFlavor fuzzy_flavor) noexcept;
inline std::string to_string(const FuzzyFlavor& ff);

constexpr std::underlying_type_t<FuzzyFlavor> to_underlying(FuzzyFlavor fuzzy_flavor) noexcept {
  return static_cast<std::underlying_type_t<FuzzyFlavor>>(fuzzy_flavor);
}

std::string to_string(const FuzzyFlavor& ff) {
  switch (ff) {
    case FuzzyFlavor::VECTOR : return "VECTOR";
    case FuzzyFlavor::DISCRETE_DOUBLE_TABLE : return "DISCRETE_DOUBLE_TABLE";
  }
}



class FuzzyFactory_7 {
 public:
  FuzzyFactory_7(FuzzyFlavor flavor, int pool_size, int sampling, int pop_size = -1);
  ~FuzzyFactory_7();

  void init(int pop_size);
  void stats();
  AbstractFuzzy* get_fuzzy();
  void give_back(AbstractFuzzy* fuzzy);

 private:
  AbstractFuzzy* createFuzzy();

  std::list<AbstractFuzzy*> list_unused_fuzzy_;
  std::vector<std::list<AbstractFuzzy*>> local_list_unused_fuzzy_;

  FuzzyFlavor flavor_;
  size_t pool_size_;

  size_t global_pool_size_ = 256;
  size_t local_pool_size_ = 256;

  int32_t nb_local_pool = 0;

  int32_t PHENOTYPE_VECTOR_SIZE = -1;
  double D_PHENOTYPE_VECTOR_SIZE = ((double)PHENOTYPE_VECTOR_SIZE);
};

}
#endif //AEVOL_DNAFACTORY_H
