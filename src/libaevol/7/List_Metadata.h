// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_LIST_METADATA_H
#define AEVOL_LIST_METADATA_H

#include "ExpManager_7.h"
#include "Protein_7.h"
#include "Rna_7.h"
#include "Dna_7.h"
#include "biochemistry/Strand.h"


#include <algorithm>

using std::list;


namespace aevol {

class Individual_7;

class List_Metadata {
    public:
     List_Metadata(Individual_7* indiv, List_Metadata* metadata);
     List_Metadata(Individual_7* indiv);

     ~List_Metadata();

     /*** Promoters ***/
     static int8_t is_promoter_leading(const Dna_7& dna, int pos);
     static int8_t is_promoter_lagging(const Dna_7& dna, int pos);

     /*** Terminators ***/
     #ifdef BASE_4
       bool is_terminator_leading(int pos);
       bool is_terminator_lagging(int pos);
     #endif

     /*** Shine Dal + Start Codon ***/
     int8_t is_shine_dal_start_prot_leading(int pos);
     int8_t is_shine_dal_start_prot_lagging(int pos);

     void clean_remote();

#ifdef __REGUL
        void add_inherited_proteins();
#endif
        /** Getter **/

        void set_iterators();

        /*** RNAs ***/
        Rna_7* rnas(int idx);
        void rna_add(int idx, Rna_7* rna, Promoter* prom = nullptr);
        void rna_add(int idx, int32_t t_begin, int32_t t_end,
                     Strand strand, double t_e,
                     int32_t t_length,
                     Promoter* prom = nullptr);

        Rna_7* rna_next();
        void rna_begin();
        bool rna_end();

        int rna_count();

        void rnas_clear();

        /*** Proteins ***/
        Protein_7* proteins(int idx);
        void protein_add(int idx, Protein_7* prot);
        void protein_add(int32_t t_protein_start,
            int32_t t_protein_end,
            int32_t t_protein_length,
            Strand strand,
            double t_e, Rna_7* rna);

        Protein_7* protein_next();
        void protein_begin();
        bool protein_end();

        int proteins_count();
        void set_proteins_count(int pcount);

        void proteins_resize(int resize);
        void proteins_clear();

        void proteins_remove_signal();

        void proteins_print(int step = -1, int indiv_id = -1);

        /** Search and update **/

        int32_t length();


    #ifdef __REGUL
        void reinit_proteins_inherited();
    #endif

  int32_t rna_count_ = 0;
  Individual_7* indiv_;

        std::list<Protein_7*> proteins_;
#ifdef __REGUL
  std::list<Protein_7*> inherited_proteins_;
  std::vector<Protein_7*> signal_proteins_;
  std::list<Protein_7*> proteins_translated_;
  bool inherited_added_ = false;
#endif
        std::list<Rna_7*> rnas_;
 protected:
        std::list<Rna_7*>::iterator it_rna_;
        std::list<Protein_7*>::iterator it_protein_;

        std::set<int> terminator_lag_;
        std::set<int> terminator_lead_;


        int32_t protein_count_ = 0;
        int32_t rna_count_add_ = 0;

        int cmp_rna = 0;

    };

}


#endif //AEVOL_LIST_METADATA_H
