// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PROMOTER_H
#define AEVOL_PROMOTER_H

#include <cstdint>
#include <map>

#include "Strand.h"

namespace aevol {
  class Rna_7;

class Promoter {
 public:
  Promoter(int32_t t_pos, int8_t t_error, Strand strand) {
    pos                = t_pos;
    error              = t_error;
    strand_ = strand;
  }

  explicit Promoter(const Promoter& clone) {
    pos                = clone.pos;
    error              = clone.error;
    strand_ = clone.strand_;
  }

  explicit Promoter(Promoter* clone) {
    pos                = clone->pos;
    error              = clone->error;
    strand_ = clone->strand_;
  }

  int32_t pos  = -1;
  int8_t error = -1;
  Strand strand_;


  bool to_compute_end_ = true;
  Rna_7* rna = nullptr;
};

}

#endif //AEVOL_PROMOTER_H
