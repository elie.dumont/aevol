// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_AEVOL_H_
#define AEVOL_AEVOL_H_

#include "AeTime.h"
#include "Alignment.h"
#include "Codon.h"
#include "Deletion.h"
#include "legacy/biochemistry/Dna.h"
#include "DnaReplicationReport.h"
#include "Dump.h"
#include "Duplication.h"
#include "legacy/ExpManager.h"
#include "ExpSetup.h"
#include "legacy/phenotype/fuzzy/Fuzzy.h"
#include "Gaussian.h"
#include "GeneticUnit.h"
#include "GridCell.h"
#include "Habitat.h"
#include "legacy/population/Individual.h"
#include "legacy/population/IndividualFactory.h"
#include "Inversion.h"
#include "JumpPoly.h"
#include "JumpingMT.h"
#include "LightTree.h"
#include "Logging.h"
#include "Metrics.h"
#include "Mutation.h"
#include "MutationParams.h"
#include "NonCodingMetrics.h"
#include "OutputManager.h"
#include "Phenotype.h"
#include "PhenotypicSegment.h"
#include "PhenotypicTarget.h"
#include "PhenotypicTargetHandler.h"
#include "Point.h"
#include "PointMutation.h"
#include "legacy/biochemistry/Protein.h"
#include "ReplicationReport.h"
#include "legacy/biochemistry/Rna.h"
#include "Selection.h"
#include "SmallDeletion.h"
#include "SmallInsertion.h"
#include "StatRecord.h"
#include "legacy/io/stats/Stats.h"
#include "Strand.h"
#include "Translocation.h"
#include "Tree.h"
#include "Utils.h"
#include "VisAVis.h"
#include "World.h"
#include "ae_enums.h"
#include "ae_string.h"
#include "macros.h"
#include "parameters/ParamReader.h"
#include "parameters/ParameterLine.h"

#ifdef __X11
  #include "ExpManager_X11.h"
  #include "Individual_X11.h"
  #include "X11Window.h"
#endif

#endif // AEVOL_AEVOL_H_
