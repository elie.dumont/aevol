// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_STRAND_H_
#define AEVOL_STRAND_H_

#include <cstdint>
#include <string>
#include <type_traits>

namespace aevol {

enum class Strand : uint8_t {
  LEADING = 0,
  LAGGING = 1
};

// Tmp non-template implem of C++23 std::to_underlying
inline constexpr std::underlying_type_t<Strand> to_underlying(Strand strand) noexcept;
inline std::string to_string(const Strand& s);

constexpr std::underlying_type_t<Strand> to_underlying(Strand strand) noexcept {
  return static_cast<std::underlying_type_t<Strand>>(strand);
}

std::string to_string(const Strand& s) {
  return s == Strand::LEADING ? "LEADING" : "LAGGING";
}

}  // namespace aevol

#endif  // AEVOL_STRAND_H_
