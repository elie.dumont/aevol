// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ParamLoader.h"

#include "ExpSetup.h"
#include "Habitat.h"
#include "legacy/population/Individual.h"
#ifdef __REGUL
  #include "legacy/raevol/Individual_R.h"
#endif
#include "legacy/population/IndividualFactory.h"
#include "JumpingMT.h"
#include "MutationParams.h"
#include "OutputManager.h"
#include "PhenotypicTargetHandler.h"
#include "Selection.h"
#include "World.h"

namespace aevol {

void ParamLoader::load(const ParamValues& param_values,
                       ExpManager* exp_m,
                       bool verbose,
                       char* chromosome,
                       int32_t lchromosome
                       #ifdef HAVE_MPI
                         , int32_t nb_rank
                       #endif
) {
#ifdef HAVE_MPI
  global_pop_size_ = init_pop_size_;
  global_grid_width_ = grid_width_;
  global_grid_height_ = grid_height_;
  rank_width_ = 1;
  rank_height_ = 1;

  int min_dist = grid_width_;
  printf("Init width rank %d (global_pop_size %d)\n",nb_rank,global_pop_size_%1);

  for (int32_t i = 2; i <= nb_rank; i++) {
    printf("Testing width rank %d (global_pop_size %d)\n",i,global_pop_size_%i);
    if (global_pop_size_ % i == 0) {
      int32_t seg_rank_width_ = i;
      int32_t seg_rank_height_ = nb_rank / i;

      if (seg_rank_height_ * seg_rank_width_ > nb_rank) {
        printf("Incorrect Nb Rank %d %d (%d)\n",seg_rank_width_,seg_rank_height_,nb_rank);
        continue;
      }

      int32_t seg_grid_width = global_grid_width_ / seg_rank_width_;
      int32_t seg_grid_height = global_grid_height_ / seg_rank_height_;

      if (seg_grid_width*seg_grid_height*nb_rank != init_pop_size_) {
        printf("Invalid population size %d (%d %d) Nb Rank %d Pop Size %d\n",seg_grid_width*seg_grid_height,seg_grid_width,seg_grid_height,nb_rank,init_pop_size_);
        continue;
      }

      printf("Evaluating %d x %d : %d (%d x %d) dist %d (min dist %d)\n",seg_rank_width_,seg_rank_height_,
             seg_grid_width*seg_grid_height,
             seg_grid_width,seg_grid_height,
             std::abs(seg_grid_width - seg_grid_height),min_dist);
      if (std::abs(seg_grid_width - seg_grid_height) < min_dist) {
        grid_width_ = seg_grid_width;
        grid_height_ = seg_grid_height;
        min_dist = std::abs(seg_grid_width - seg_grid_height);
        init_pop_size_ = grid_width_*grid_height_;
        rank_width_ = seg_rank_width_;
        rank_height_ = seg_rank_height_;
      }
    }
  }

  printf("Global population is %d (%d x %d) : Patch population is %d (%d x %d)\n",
         global_pop_size_,global_grid_width_,global_grid_height_,
         init_pop_size_,grid_width_,grid_height_);
#endif
  // Initialize master prng
  // Will be used to create the initial genome(s) and to generate seeds for the other prngs
  auto prng = std::make_shared<JumpingMT>(param_values.seed_);

  // Initialize mut_prng, stoch_prng, world_prng :
  // if mut_seed (respectively stoch_seed) not given in param.in, choose it at random
  int32_t mut_seed = (param_values.mut_seed_ != 0) ? param_values.mut_seed_
                                                   : prng->random(1000000);
  int32_t stoch_seed = (param_values.stoch_seed_ != 0) ? param_values.stoch_seed_
                                                       : prng->random(1000000);
  auto mut_prng   = std::make_shared<JumpingMT>(mut_seed);
  auto stoch_prng = std::make_shared<JumpingMT>(stoch_seed);
  auto world_prng = std::make_shared<JumpingMT>(prng->random(1000000));

  // Create aliases
  ExpSetup* exp_s = exp_m->exp_s();
  Selection* sel = exp_m->sel();
  OutputManager* output_m = exp_m->output_m();
  output_m->InitStats();


  // 1) ------------------------------------- Initialize the experimental setup


  // ---------------------------------------------------------------- Selection
  sel->set_selection_scheme(param_values.selection_scheme_);
  sel->set_selection_pressure(param_values.selection_pressure_);

  sel->set_selection_scope(param_values.selection_scope_);
  sel->set_selection_scope_x(param_values.selection_scope_x_);
  sel->set_selection_scope_y(param_values.selection_scope_y_);

  sel->set_fitness_function(param_values.fitness_function_);
  sel->set_fitness_function_scope_x(param_values.fitness_function_x_);
  sel->set_fitness_function_scope_y(param_values.fitness_function_y_);
  // ----------------------------------------------------------------- Transfer
  exp_s->set_with_HT(param_values.with_HT_);
  exp_s->set_repl_HT_with_close_points(param_values.repl_HT_with_close_points_);
  exp_s->set_HT_ins_rate(param_values.HT_ins_rate_);
  exp_s->set_HT_repl_rate(param_values.HT_repl_rate_);
  exp_s->set_repl_HT_detach_rate(param_values.repl_HT_detach_rate_);

  // ---------------------------------------------------------------- Secretion
  exp_s->set_with_secretion(param_values.with_secretion_);
  exp_s->set_secretion_contrib_to_fitness(param_values.secretion_contrib_to_fitness_);
  exp_s->set_secretion_cost(param_values.secretion_cost_);

  exp_s->set_fuzzy_flavor(param_values._fuzzy_flavor);
  exp_s->set_simd_metadata_flavor(param_values.simd_metadata_flavor_);


  //------------------------------------------------------------------ Parameter for SIMD
  exp_s->set_min_genome_length(param_values.min_genome_length_);
  exp_s->set_max_genome_length(param_values.max_genome_length_);

#ifdef BASE_4
  // ----------------------------------------------------------------- Terminators
  exp_s->set_terminator_polya_sequence_length(param_values.term_polya_seq_length_);

  // -------------------------------------------------- MWH bases configuration
  if(param_values.mwh_bases_redefined_) {
    exp_s->set_aa_base_m(param_values.aa_base_m_);
    exp_s->set_aa_base_w(param_values.aa_base_w_);
    exp_s->set_aa_base_h(param_values.aa_base_h_);
  }
#endif

#ifdef __REGUL
  if (param_values.env_var_method_ == ONE_AFTER_ANOTHER)
    exp_s->set_with_heredity(false);
  else
    exp_s->set_with_heredity(param_values._with_heredity);

  exp_s->set_degradation_rate(param_values._degradation_rate);
  exp_s->set_nb_degradation_step(param_values._nb_degradation_step);
  exp_s->set_nb_indiv_age(param_values._nb_indiv_age);
  exp_s->set_list_eval_step(param_values._list_eval_step);
  exp_s->set_protein_presence_limit(param_values._protein_presence_limit);
  exp_s->set_hill_shape(pow( param_values._hill_shape_theta, param_values._hill_shape_n ));
  exp_s->set_hill_shape_n( param_values._hill_shape_n );

  // printf("Random %d : Sparsity %lf\n",_random_binding_matrix,_binding_zeros_percentage);
  exp_s->init_binding_matrix(param_values._random_binding_matrix, param_values._binding_zeros_percentage, prng);
#endif

#ifdef HAVE_MPI
  //------------------------------------------------------ Global Grid characteristics
  exp_s->set_global_pop_size(global_pop_size_);
  exp_s->set_global_grid_width(global_grid_width_);
  exp_s->set_global_grid_height(global_grid_height_);

  exp_s->set_rank_width(rank_width_);
  exp_s->set_rank_height(rank_height_);
#endif

// 2) --------------------------------------------- Create and init a Habitat
#ifndef __REGUL
  Habitat habitat;
#else
  Habitat_R habitat;
#endif

// Shorthand for phenotypic target handler
#ifndef __REGUL
  PhenotypicTargetHandler& phenotypic_target_handler =
      habitat.phenotypic_target_handler_nonconst();
#else
  PhenotypicTargetHandler_R& phenotypic_target_handler =
      habitat.phenotypic_target_handler_nonconst();
#endif

// Move the gaussian list from the parameters to the phen target handler
#ifndef __REGUL
  phenotypic_target_handler.set_gaussians(param_values.std_env_gaussians);
#else
  phenotypic_target_handler.set_gaussians(param_values._env_gaussians_list);
  phenotypic_target_handler.set_signals_models(param_values._signals_models);
  phenotypic_target_handler.set_signals(param_values._env_signals_list);
#endif

  // Copy the sampling
  phenotypic_target_handler.set_sampling(param_values.env_sampling_);

  // Set phenotypic target segmentation

  if((param_values.env_axis_features_ != NULL) && (param_values.env_axis_segment_boundaries_ != NULL)) {
    // if param.in contained a line starting with ENV_AXIS_FEATURES,
    // we use the values indicated on this line
    phenotypic_target_handler.set_segmentation(param_values.env_axis_nb_segments_,
                                               param_values.env_axis_segment_boundaries_,
                                               param_values.env_axis_features_);
  }
  // else we leave the segmentation as it is by default
  // (one "metabolic" segment from X_MIN to X_MAX)


  // Set phenotypic target variation
  if (param_values.env_var_method_ != NO_VAR)
  {
    phenotypic_target_handler.set_var_method(param_values.env_var_method_);
    phenotypic_target_handler.set_var_prng(std::make_shared<JumpingMT>(param_values.env_var_seed_));
    phenotypic_target_handler.set_var_sigma_tau(param_values.env_var_sigma_, param_values.env_var_tau_);
#ifdef __REGUL
    phenotypic_target_handler.set_switch_probability(param_values._env_switch_probability);
#endif
  }

  // Set phenotypic target noise
  if (param_values.env_noise_method_ != NO_NOISE)
  {
    phenotypic_target_handler.set_noise_method(param_values.env_noise_method_);
    phenotypic_target_handler.set_noise_sampling_log(param_values.env_noise_sampling_log_);
    phenotypic_target_handler.set_noise_prng(std::make_shared<JumpingMT>(param_values.env_noise_seed_));
    phenotypic_target_handler.set_noise_alpha(param_values.env_noise_alpha_);
    phenotypic_target_handler.set_noise_sigma(param_values.env_noise_sigma_);
    phenotypic_target_handler.set_noise_prob(param_values.env_noise_prob_);
  }

// Build the phenotypic target
#ifndef __REGUL
  phenotypic_target_handler.BuildPhenotypicTarget();
#else
  printf("Init phenotypic target with %d\n", param_values._nb_indiv_age);
  phenotypic_target_handler.InitPhenotypicTargetsAndModels( param_values._nb_indiv_age );
#endif

  if (verbose) {
#ifndef __REGUL
    printf("Entire geometric area of the phenotypic target : %f\n",
           phenotypic_target_handler.get_geometric_area());
#else
    phenotypic_target_handler.print_geometric_areas();
#endif
  }


  // 3) --------------------------------------------- Create the new population
  list<Individual *> indivs;
  // Generate a model ae_mut_param object
  auto param_mut = std::make_shared<MutationParams>();
  param_mut->set_point_mutation_rate(param_values.point_mutation_rate_);
  param_mut->set_small_insertion_rate(param_values.small_insertion_rate_);
  param_mut->set_small_deletion_rate(param_values.small_deletion_rate_);
  param_mut->set_max_indel_size(param_values.max_indel_size_);
  param_mut->set_with_4pts_trans(param_values.with_4pts_trans_);
  param_mut->set_with_alignments(param_values.with_alignments_);
  param_mut->set_with_HT(param_values.with_HT_);
  param_mut->set_repl_HT_with_close_points(param_values.repl_HT_with_close_points_);
  param_mut->set_HT_ins_rate(param_values.HT_ins_rate_);
  param_mut->set_HT_repl_rate(param_values.HT_repl_rate_);
  param_mut->set_repl_HT_detach_rate(param_values.repl_HT_detach_rate_);
  param_mut->set_duplication_rate(param_values.duplication_rate_);
  param_mut->set_deletion_rate(param_values.deletion_rate_);
  param_mut->set_translocation_rate(param_values.translocation_rate_);
  param_mut->set_inversion_rate(param_values.inversion_rate_);
  param_mut->set_neighbourhood_rate(param_values.neighbourhood_rate_);
  param_mut->set_duplication_proportion(param_values.duplication_proportion_);
  param_mut->set_deletion_proportion(param_values.deletion_proportion_);
  param_mut->set_translocation_proportion(param_values.translocation_proportion_);
  param_mut->set_inversion_proportion(param_values.inversion_proportion_);

  exp_s->set_mutation_parameters(param_mut);

  Individual * indiv = NULL;
  int32_t id_new_indiv = 0;

  if (chromosome != NULL)
  {
    printf("Option -c is used: chromosome will be loaded from a text file\n");
#ifndef __REGUL
    Individual * indiv = new Individual(exp_m,
                                       mut_prng,
                                       stoch_prng,
                                       param_mut,
                                       param_values.w_max_,
                                       param_values.min_genome_length_,
                                       param_values.max_genome_length_,
                                       id_new_indiv++,
                                       param_values.strain_name_,
                                       0);
#else
    Individual_R * indiv = new Individual_R(exp_m,
                                           mut_prng,
                                           stoch_prng,
                                           param_mut,
                                           param_values.w_max_,
                                           param_values.min_genome_length_,
                                           param_values.max_genome_length_,
                                           id_new_indiv++,
                                           param_values.strain_name_,
                                           0);

#endif

    indiv->add_GU(chromosome, lchromosome);
    indiv->genetic_unit_nonconst(0).set_min_gu_length(param_values.chromosome_minimal_length_);
    indiv->genetic_unit_nonconst(0).set_max_gu_length(param_values.chromosome_maximal_length_);

    indiv->set_with_stochasticity(param_values.with_stochasticity_);
    indiv->compute_statistical_data();
    indiv->EvaluateInContext(habitat);
    printf("Starting with a clonal population of individual with metabolic error %f and secretion error %f \n",
           indiv->dist_to_target_by_feature(METABOLISM),
           indiv->dist_to_target_by_feature(SECRETION));
    indivs.push_back(indiv);

    // Make the clones and add them to the list of individuals
    for (int32_t i = 1 ; i < param_values.init_pop_size_ ; i++)
    {
#ifndef __REGUL
      Individual * clone = Individual::CreateClone(indiv, id_new_indiv++);
#else
      Individual_R * clone = Individual_R::CreateClone(indiv, id_new_indiv++);
#endif
      clone->EvaluateInContext(habitat);
      indivs.push_back(clone);
    }
  }
  else if (param_values.init_method_ & ONE_GOOD_GENE)
  {
    if (param_values.init_method_ & CLONE)
    {
      // Create an individual with a "good" gene (in fact, make an indiv whose
      // fitness is better than that corresponding to a flat phenotype)
      // and set its id
      indiv = IndividualFactory::create_random_individual(
          exp_m,
          id_new_indiv++,
          param_mut,
          mut_prng,
          stoch_prng,
          habitat,
          param_values.w_max_,
          param_values.min_genome_length_,
          param_values.max_genome_length_,
          param_values.chromosome_initial_length_,
          param_values.strain_name_,
          prng,
          true);
      indiv->genetic_unit_nonconst(0).set_min_gu_length(param_values.chromosome_minimal_length_);
      indiv->genetic_unit_nonconst(0).set_max_gu_length(param_values.chromosome_maximal_length_);

      indiv->set_with_stochasticity(param_values.with_stochasticity_);

      // Add it to the list
      indivs.push_back(indiv);

      // Make the clones and add them to the list of individuals
      for (int32_t i = 1 ; i < param_values.init_pop_size_ ; i++)
      {
// Add new clone to the list
#ifndef __REGUL
        Individual * clone = Individual::CreateClone(indiv, id_new_indiv++);
#else
        Individual_R * clone = Individual_R::CreateClone(dynamic_cast<Individual_R*>(indiv), id_new_indiv++);
#endif
        clone->EvaluateInContext(habitat);
        indivs.push_back(clone);
      }
    }
    else // if (! CLONE)
    {
      for (int32_t i = 0 ; i < param_values.init_pop_size_ ; i++)
      {
        // Create an individual and set its id
        indiv = IndividualFactory::create_random_individual(
            exp_m,
            id_new_indiv++,
            param_mut,
            mut_prng,
            stoch_prng,
            habitat,
            param_values.w_max_,
            param_values.min_genome_length_,
            param_values.max_genome_length_,
            param_values.chromosome_initial_length_,
            param_values.strain_name_,
            prng,
            true);
        indiv->genetic_unit_nonconst(0).set_min_gu_length(param_values.chromosome_minimal_length_);
        indiv->genetic_unit_nonconst(0).set_max_gu_length(param_values.chromosome_maximal_length_);

        // Add it to the list
        indivs.push_back(indiv);
      }
    }
  }
  else // if (! ONE_GOOD_GENE)
  {
    if (param_values.init_method_ & CLONE)
    {
      // Create a random individual and set its id
      indiv = IndividualFactory::create_random_individual(
          exp_m,
          id_new_indiv++,
          param_mut,
          mut_prng,
          stoch_prng,
          habitat,
          param_values.w_max_,
          param_values.min_genome_length_,
          param_values.max_genome_length_,
          param_values.chromosome_initial_length_,
          param_values.strain_name_,
          prng,
          false);
      indiv->genetic_unit_nonconst(0).set_min_gu_length(param_values.chromosome_minimal_length_);
      indiv->genetic_unit_nonconst(0).set_max_gu_length(param_values.chromosome_maximal_length_);

      indiv->clear_everything_except_dna_and_promoters();
      indiv->EvaluateInContext(habitat);

      // Add it to the list
      indivs.push_back(indiv);

      // Make the clones and add them to the list of individuals
      for (int32_t i = 1 ; i < param_values.init_pop_size_ ; i++)
      {
// Add clone to the list
#ifndef __REGUL
        Individual * clone = Individual::CreateClone(indiv, id_new_indiv++);
#else
        Individual_R * clone = Individual_R::CreateClone(dynamic_cast<Individual_R*>(indiv), id_new_indiv++);
#endif
        clone->EvaluateInContext(habitat);
        indivs.push_back(clone);
      }
    }
    else // if (! CLONE)
    {
      for (int32_t i = 0 ; i < param_values.init_pop_size_ ; i++)
      {
        // Create a random individual and set its id
        indiv = IndividualFactory::create_random_individual(
            exp_m,
            id_new_indiv++,
            param_mut,
            mut_prng,
            stoch_prng,
            habitat,
            param_values.w_max_,
            param_values.min_genome_length_,
            param_values.max_genome_length_,
            param_values.chromosome_initial_length_,
            param_values.strain_name_,
            prng,
            false);
        indiv->genetic_unit_nonconst(0).set_min_gu_length(param_values.chromosome_minimal_length_);
        indiv->genetic_unit_nonconst(0).set_max_gu_length(param_values.chromosome_maximal_length_);

        // Add it to the list
        indivs.push_back(indiv);
      }
    }
  }

// -------------------------------------------------------- Spatial structure
#ifdef HAVE_MPI
  exp_m->mut_prng_seed_ = new int32_t[param_values.global_pop_size_];
  exp_m->stoch_prng_seed_ = new int32_t[param_values.global_pop_size_];

  for (int16_t x = 0; x < param_values.global_grid_width_; x++) {
    for (int16_t y = 0; y < param_values.global_grid_height_; y++) {
      exp_m->mut_prng_seed_[x * param_values.global_grid_height_+y]  = mut_prng->random(1000000);
      exp_m->stoch_prng_seed_[x * param_values.global_grid_height_+y] = stoch_prng->random(1000000);
    }
  }
#endif

  exp_m->InitializeWorld(param_values.grid_width_, param_values.grid_height_,
                         world_prng, mut_prng, stoch_prng,
                         habitat,
                         true);
  World* world = exp_m->world();

#ifdef HAVE_MPI
  exp_m->prng_seed_ = new int32_t[param_values.global_pop_size_];

  for (int16_t x = 0; x < param_values.global_grid_width_; x++) {
    for (int16_t y = 0; y < param_values.global_grid_height_; y++) {
      exp_m->prng_seed_[x * param_values.global_grid_height_+y] = prng_->random(1000000);
    }
  }
#endif

  for (int16_t x = 0; x < exp_m->grid_width(); x++) {
    for (int16_t y = 0; y < exp_m->grid_height(); y++) {
#ifdef HAVE_MPI
      int32_t seed = exp_m->prng_seed_[x*exp_m->grid_height()+y];
#else
      int32_t seed = prng->random(1000000);
#endif
#if __cplusplus == 201103L
      exp_m->world()->grid(x,y)->set_reprod_prng(make_unique<JumpingMT>(seed));
      exp_m->world()->grid(x,y)->set_reprod_prng_simd(make_unique<JumpingMT>(seed));
#else
      exp_m->world()->grid(x,y)->set_reprod_prng(std::make_unique<JumpingMT>(seed));
      exp_m->world()->grid(x,y)->set_reprod_prng_simd(std::make_unique<JumpingMT>(seed));
#endif
    }
  }

  world->set_secretion_degradation_prop(param_values.secretion_degradation_prop_);
  world->set_secretion_diffusion_prop(param_values.secretion_diffusion_prop_);
  world->set_is_well_mixed(param_values.well_mixed);
  world->set_partial_mix_nb_permutations(param_values.partial_mix_nb_permutations);

  // Set each individual's position on the grid
  int16_t x, y;
  int16_t x_max = exp_m->grid_width();
  int16_t y_max = exp_m->grid_height();

  for (const auto& indiv: indivs) {
    do {
      x = exp_m->world()->prng()->random(x_max);
      y = exp_m->world()->prng()->random(y_max);
    } while (world->indiv_at(x, y) != NULL);

    world->PlaceIndiv(indiv, x, y, true);
  }

  world->set_best(0, 0);



  // 4) ------------------------------------------ Set the recording parameters
  output_m->set_backup_step(param_values.backup_step_);
  output_m->set_big_backup_step(param_values.big_backup_step_);

  if (param_values.record_tree_)
  {
    output_m->init_tree(exp_m, param_values.tree_step_);
  }

  output_m->init_light_tree(param_values.record_light_tree_, param_values.tree_step_);

  if (param_values.make_dumps_)
  {
    output_m->set_dump_step(param_values.dump_step_);
  }
  output_m->set_logs(param_values.logs_);
}

}  // namespace aevol
