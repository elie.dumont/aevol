// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************




// =================================================================
//                              Libraries
// =================================================================
#include <cstring>
#include <cassert>

#include <list>
#ifdef __REGUL
  #include <vector>
#endif

// =================================================================
//                            Project Files
// =================================================================
#include "ParamReader.h"

#if __cplusplus == 201103L
#include "make_unique.h"
#endif

#include "Gaussian.h"

namespace aevol {

// =================================================================
//                          Class declarations
// =================================================================


//##############################################################################
//                                                                             #
//                             Class ParamReader                               #
//                                                                             #
//##############################################################################

// =================================================================
//                    Definition of static attributes
// =================================================================
const char kTabChar = 0x09;

// =================================================================
//                            Public Methods
// =================================================================
ParamValues ParamReader::read_file(const char* file_name) {
  ParamValues param_values;

  // Open parameter file
  FILE* file = fopen(file_name,  "r");

  if (file == NULL) {
    printf("ERROR : couldn't open file %s\n", file_name);
    exit(EXIT_FAILURE);
  }

  int32_t cur_line = 0;
  ParameterLine* parameter_line;

  // TODO : write parameter_line = new ParameterLine(param_file_) => ParameterLine::ParameterLine(char*)
  while ((parameter_line = line(file, cur_line)) != NULL) {
    interpret_line(*parameter_line, cur_line, file_name, param_values);
    delete parameter_line;
  }

  fclose(file);

  // Check consistency of parameter values
  param_values.CheckConsistency();

  return param_values;
}

// =================================================================
//                           Protected Methods
// =================================================================
/*!
  \brief Get a line in a file and format it

  \return line (pointer)

  \see format_line(ParameterLine* formated_line, char* line, bool* line_is_interpretable)
*/
ParameterLine* ParamReader::line(FILE* file, int32_t& cur_line) {
  char line[4096];
  ParameterLine* formated_line = new ParameterLine();

  bool found_interpretable_line = false; // Found line that is neither a comment nor empty

  while (!feof(file) && !found_interpretable_line)
  {
    if (!fgets(line, 4096, file))
    {
      delete formated_line;
      return NULL;
    }
    cur_line++;
    format_line(formated_line, line, &found_interpretable_line);
  }

  if (found_interpretable_line)
  {
    return formated_line;
  }
  else
  {
    delete formated_line;
    return NULL;
  }
}

/*!
  \brief Format a line by parsing it and the words inside

  \param formated_line the resulted formated line
  \param line original line in char*
  \param line_is_interpretable
*/
void ParamReader::format_line(ParameterLine* formated_line,
                              char* line,
                              bool* line_is_interpretable) {
  int32_t i = 0;
  int32_t j;

  // Parse line
  while (line[i] != '\n' && line[i] != '\0' && line[i] != '\r')
  {
    j = 0;

    // Flush white spaces and tabs
    while (line[i] == ' ' || line[i] == kTabChar) i++;

    // Check comments
    if (line[i] == '#') break;

    // If we got this far, there is content in the line
    *line_is_interpretable = true;

    // Parse word
    while (line[i] != ' ' && line[i] != kTabChar && line[i] != '\n' &&
           line[i] != '\0' && line[i] != '\r') {
      formated_line->words[formated_line->nb_words][j++] = line[i++];
    }

    // Add '\0' at end of word if it's not empty (line ending with space or tab)
    if (j != 0)
    {
      formated_line->words[formated_line->nb_words++][j] = '\0';
    }
  }
}

void ParamReader::interpret_line(const ParameterLine& line,
                                 int32_t cur_line,
                                 const char* file_name,
                                 ParamValues& values) {
  if (strcmp(line.words[0], "STRAIN_NAME") == 0)
  {
    delete [] values.strain_name_;
    values.strain_name_ = new char[strlen(line.words[1])+1];
    strcpy(values.strain_name_, line.words[1]);
  }
  else if (strcmp(line.words[0], "MIN_TRIANGLE_WIDTH") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "its value is fixed to 0.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "MAX_TRIANGLE_WIDTH") == 0)
  {
    values.w_max_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "ENV_AXIS_FEATURES") == 0)
  {
    // Set general segmentation data
    values.env_axis_nb_segments_ = line.nb_words / 2;

    // Set segmentation boundaries
    values.env_axis_segment_boundaries_ = new double [values.env_axis_nb_segments_ + 1];
    values.env_axis_segment_boundaries_[0] = X_MIN;
    for (int16_t i = 1 ; i < values.env_axis_nb_segments_ ; i++)
    {
      values.env_axis_segment_boundaries_[i] = atof(line.words[2*i]);
    }
    values.env_axis_segment_boundaries_[values.env_axis_nb_segments_] = X_MAX;

    // Set segment features
    values.env_axis_features_ = new PhenotypicFeature[values.env_axis_nb_segments_];
    for (int16_t i = 0 ; i < values.env_axis_nb_segments_ ; i++)
    {
      if (strcmp(line.words[2*i+1], "NEUTRAL") == 0)
      {
        values.env_axis_features_[i] = NEUTRAL;
      }
      else if (strcmp(line.words[2*i+1], "METABOLISM") == 0)
      {
        values.env_axis_features_[i] = METABOLISM;
      }
      else if (strcmp(line.words[2*i+1], "SECRETION") == 0)
      {
        values.with_secretion_ = true;
        values.env_axis_features_[i] = SECRETION;
      }
      else if (strcmp(line.words[2*i+1], "DONOR") == 0)
      {
        values.env_axis_features_[i] = DONOR;
      }
      else if (strcmp(line.words[2*i+1], "RECIPIENT") == 0)
      {
        values.env_axis_features_[i] = RECIPIENT;
      }
      else
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": unknown axis feature \"%s\".\n",
               file_name, cur_line, line.words[2*i+1]);
        exit(EXIT_FAILURE);
      }
    }
  }
  else if (strcmp(line.words[0], "ENV_SEPARATE_SEGMENTS") == 0)
  {
    values.env_axis_separate_segments_ = true;
  }
  else if (strcmp(line.words[0], "RECORD_TREE") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.record_tree_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.record_tree_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown tree recording option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "TREE_MODE") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32 ": "
           "Tree mode management has been removed.\n",
           file_name, cur_line);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "RECORD_LIGHT_TREE") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.record_light_tree_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.record_light_tree_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
             ": unknown light tree recording option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "MORE_STATS") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.more_stats_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.more_stats_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown more stats option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "DUMP_PERIOD") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use DUMP_STEP instead.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "DUMP_STEP") == 0)
  {
    values.dump_step_ = atol(line.words[1]);
    if (values.dump_step_ > 0) values.make_dumps_ = true;
  }
  else if (strcmp(line.words[0], "BACKUP_STEP") == 0)
  {
    values.backup_step_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "BIG_BACKUP_STEP") == 0)
  {
    values.big_backup_step_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "TREE_STEP") == 0)
  {
    values.tree_step_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "NB_GENER") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use command line arguments of aevol_run instead.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "INITIAL_GENOME_LENGTH") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use CHROMOSOME_INITIAL_LENGTH instead.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "CHROMOSOME_INITIAL_LENGTH") == 0)
  {
    values.chromosome_initial_length_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "MIN_GENOME_LENGTH") == 0)
  {
    if (strncmp(line.words[1], "NONE", 4) == 0)
    {
      values.min_genome_length_ = 1; // Must not be 0
    }
    else
    {
      values.min_genome_length_ = atol(line.words[1]);
      if (values.min_genome_length_ == 0)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : MIN_GENOME_LENGTH must be > 0.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }
    }
  }
  else if (strcmp(line.words[0], "MAX_GENOME_LENGTH") == 0)
  {
    if (strncmp(line.words[1], "NONE", 4) == 0)
    {
      values.max_genome_length_ = INT32_MAX;
    }
    else
    {
      values.max_genome_length_ = atol(line.words[1]);
    }
  }
  else if (strcmp(line.words[0], "INIT_POP_SIZE") == 0)
  {
    values.init_pop_size_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "WORLD_SIZE") == 0)
  {
    values.grid_width_ = atoi(line.words[1]);
    values.grid_height_ = atoi(line.words[2]);
  }
  else if (strcmp(line.words[0], "POP_STRUCTURE") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use WORLD_SIZE <width> <height> instead.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "MIGRATION_NUMBER") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use INDIV_MIXING instead.\n",
           file_name, cur_line, line.words[0]);
    printf("usage: INDIV_MIXING WELL_MIXED|NONE|PARTIAL <n>\n");
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "INDIV_MIXING") == 0)
  {
    if (strcmp(line.words[1], "WELL_MIXED") == 0)
      values.well_mixed = true;
    else if (strcmp(line.words[1], "NONE") == 0)
      values.well_mixed = false;
    else if (strcmp(line.words[1], "PARTIAL") == 0)
      values.partial_mix_nb_permutations = atol(line.words[2]);
    else {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown mixing option.\n", file_name, cur_line);
      printf("usage: INDIV_MIXING WELL_MIXED|NONE|PARTIAL <n>\n");
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "INIT_METHOD") == 0)
  {
    for (int8_t i = 1 ; i < line.nb_words ; i++)
    {
      if (strcmp(line.words[i], "ONE_GOOD_GENE") == 0)
      {
        values.init_method_ |= ONE_GOOD_GENE;
      }
      else if (strcmp(line.words[i], "CLONE") == 0)
      {
        values.init_method_ |= CLONE;
      }
      else if (strcmp(line.words[i], "WITH_INS_SEQ") == 0)
      {
        values.init_method_ |= WITH_INS_SEQ;
      }
      else
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": unknown initialization method %s.\n",
               file_name, cur_line, line.words[1]);
        exit(EXIT_FAILURE);
      }
    }
  }
  else if (strcmp(line.words[0], "POINT_MUTATION_RATE") == 0)
  {
    values.point_mutation_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SMALL_INSERTION_RATE") == 0)
  {
    values.small_insertion_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SMALL_DELETION_RATE") == 0)
  {
    values.small_deletion_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "MAX_INDEL_SIZE") == 0)
  {
    values.max_indel_size_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "DUPLICATION_RATE") == 0)
  {
    values.duplication_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "DELETION_RATE") == 0)
  {
    values.deletion_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "TRANSLOCATION_RATE") == 0)
  {
    values.translocation_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "INVERSION_RATE") == 0)
  {
    values.inversion_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "NEIGHBOURHOOD_RATE") == 0)
  {
    values.neighbourhood_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "DUPLICATION_PROPORTION") == 0)
  {
    values.duplication_proportion_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "DELETION_PROPORTION") == 0)
  {
    values.deletion_proportion_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "TRANSLOCATION_PROPORTION") == 0)
  {
    values.translocation_proportion_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "INVERSION_PROPORTION") == 0)
  {
    values.inversion_proportion_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "ALIGN_FUNCTION") == 0)
  {
    if (line.nb_words != 2 && line.nb_words != 4)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": incorrect number of parameters for keyword \"%s\".\n",
             file_name, cur_line, line.words[0]);
      exit(EXIT_FAILURE);
    }

    if (strcmp(line.words[1], "LINEAR") == 0)
    {
      values.align_fun_shape_ = LINEAR;

      if (line.nb_words == 4)
      {
        values.align_lin_min_ = atol(line.words[2]);
        values.align_lin_max_ = atol(line.words[3]);
      }
    }
    else if (strcmp(line.words[1], "SIGMOID") == 0)
    {
      values.align_fun_shape_ = SIGMOID;

      if (line.nb_words == 4)
      {
        values.align_sigm_lambda_ = atol(line.words[2]);
        values.align_sigm_mean_ = atol(line.words[3]);
      }
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown align function shape \"%s\".\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "ALIGN_MAX_SHIFT") == 0)
  {
    values.align_max_shift_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "ALIGN_W_ZONE_H_LEN") == 0)
  {
    values.align_w_zone_h_len_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "ALIGN_MATCH_BONUS") == 0)
  {
    values.align_match_bonus_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "ALIGN_MISMATCH_COST") == 0)
  {
    values.align_mismatch_cost_ = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "STOCHASTICITY") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.with_stochasticity_ = true;
    }
  }
  else if (strcmp(line.words[0], "SELECTION_SCHEME") == 0)
  {
    if (strncmp(line.words[1], "lin", 3) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme_ = RANK_LINEAR;
      values.selection_pressure_ = atof(line.words[2]);
    }
    else if (strncmp(line.words[1], "exp", 3) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme_ = RANK_EXPONENTIAL;
      values.selection_pressure_ = atof(line.words[2]);
    }
    else if (strncmp(line.words[1], "fitness", 7) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme_ = FITNESS_PROPORTIONATE;
      values.selection_pressure_ = atof(line.words[2]);
    }
    else if (strcmp(line.words[1], "fittest") == 0)
    {
      values.selection_scheme_ = FITTEST;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown selection scheme \"%s\".\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SELECTION_SCOPE") == 0)
  {
    if (strncmp(line.words[1], "global", 6) == 0)
    {
      values.selection_scope_ = SCOPE_GLOBAL;
    }
    else if (strncmp(line.words[1], "local", 5) == 0)
    {
      if (line.nb_words != 4)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection scope parameter for local selection is missing (x,y).\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scope_ = SCOPE_LOCAL;
      values.selection_scope_x_ = atoi(line.words[2]);
      values.selection_scope_y_ = atoi(line.words[2]);
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown selection scope \"%s\".\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SEED") == 0)
  {
    static bool seed_already_set = false;
    if (seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.seed_ = atol(line.words[1]);
    seed_already_set = true;
  }
  else if (strcmp(line.words[0], "MUT_SEED") == 0)
  {
    static bool mut_seed_already_set = false;
    if (mut_seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for MUT_SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.mut_seed_ = atol(line.words[1]);
    mut_seed_already_set = true;
  }
  else if (strcmp(line.words[0], "STOCH_SEED") == 0)
  {
    static bool stoch_seed_already_set = false;
    if (stoch_seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for STOCH_SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.stoch_seed_ = atol(line.words[1]);
    stoch_seed_already_set = true;
  }
  else if (strcmp(line.words[0], "WITH_4PTS_TRANS") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.with_4pts_trans_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      printf("ERROR: 3 points_ translocation hasn't been implemented yet\n");
      exit(EXIT_FAILURE);
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown 4pts_trans option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "WITH_ALIGNMENTS") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.with_alignments_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.with_alignments_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown alignement option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "WITH_TRANSFER") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.with_HT_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.with_HT_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown transfer option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "REPL_TRANSFER_WITH_CLOSE_POINTS") == 0)
  {
    if (strncmp(line.words[1], "true", 4) == 0)
    {
      values.repl_HT_with_close_points_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0)
    {
      values.repl_HT_with_close_points_ = false;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown transfer option (use true/false).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SWAP_GUS") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "TRANSFER_INS_RATE") == 0)
  {
    values.HT_ins_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "TRANSFER_REPL_RATE") == 0)
  {
    values.HT_repl_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "REPL_TRANSFER_DETACH_RATE") == 0)
  {
    values.repl_HT_detach_rate_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "TRANSLATION_COST") == 0)
  {
    values.translation_cost_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "ENV_ADD_POINT") == 0)
  {
    // custom_points
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": Custom points management has been removed.\n",
        file_name, cur_line);
    exit(EXIT_FAILURE);
  }
  else if ((strcmp(line.words[0], "ENV_ADD_GAUSSIAN") == 0) ||
      (strcmp(line.words[0], "ENV_GAUSSIAN") == 0))
  {
    #ifdef __REGUL
      // le premier chiffre est l'indice d'environment en convention humaine ( le premier a 1)
      // On vérifie que cet indice n'est pas trop élevé ni négatif pour éviter les crash
      if ( atoi(line.words[1]) - 1 < (int)values._env_gaussians_list.size() && atoi(line.words[1]) > 0)
      {
        (values._env_gaussians_list.at( atoi(line.words[1]) - 1)).push_back
        ( Gaussian(  atof( line.words[2] ), atof( line.words[3] ), atof( line.words[4] ) ) );
      }
      else
      {
        printf( " ERROR in param file \"%s\" on line %" PRId32 " : There is only %ld environment.\n",
         file_name, cur_line, values._env_gaussians_list.size() );
        exit( EXIT_FAILURE );
      }
    #else
      values.std_env_gaussians.push_back(
          Gaussian(atof(line.words[1]), atof(line.words[2]), atof(line.words[3])));
    #endif
  }
  else if (strcmp(line.words[0], "ENV_SAMPLING") == 0)
  {
    values.env_sampling_ = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "ENV_VARIATION") == 0)
  {
    static bool env_var_already_set = false;
    if (env_var_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : "
                 "duplicate entry for %s.\n",
             file_name, cur_line, line.words[0]);
      exit(EXIT_FAILURE);
    }
    env_var_already_set = true;

    if (strcmp(line.words[1], "none") == 0)
    {
      if (line.nb_words != 2) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s\n", line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = NO_VAR;
    }
    else if (strcmp(line.words[1], "autoregressive_mean_variation") == 0)
    {
      if (line.nb_words != 5) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s sigma tau prng_seed\n",
               line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = AUTOREGRESSIVE_MEAN_VAR;
      values.env_var_sigma_ = atof(line.words[2]);
      values.env_var_tau_ = atol(line.words[3]);
      values.env_var_seed_ = atoi(line.words[4]);
    }
    else if (strcmp(line.words[1], "autoregressive_height_variation") == 0)
    {
      if (line.nb_words != 5) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s sigma tau prng_seed\n",
               line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = AUTOREGRESSIVE_HEIGHT_VAR;
      values.env_var_sigma_ = atof(line.words[2]);
      values.env_var_tau_ = atol(line.words[3]);
      values.env_var_seed_ = atoi(line.words[4]);
    }
    else if (strcmp(line.words[1], "add_local_gaussians") == 0)
    {
      if (line.nb_words != 3) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s prng_seed\n",
               line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = LOCAL_GAUSSIANS_VAR;
      values.env_var_seed_ = atoi(line.words[2]);
    }
    #ifdef __REGUL
    else if (strcmp(line.words[1], "switch_in_a_list") == 0)
    {
      if (line.nb_words != 3) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s probability to switch between different environments\n",
               line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = SWITCH_IN_A_LIST;
      values._env_switch_probability = atof(line.words[2]);
    } else if (strcmp(line.words[1], "one_after_another") == 0)
    {
      if (line.nb_words != 2) {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": wrong number of parameters.\n",
               file_name, cur_line);
        printf("usage: %s %s probability to switch between different environments\n",
               line.words[0], line.words[1]);
        exit(EXIT_FAILURE);
      }
      values.env_var_method_ = ONE_AFTER_ANOTHER;
    }
    #endif
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 " : unknown phenotypic target variation method.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "ENV_NOISE") == 0)
  {
    static bool env_noise_already_set = false;
    if (env_noise_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 " : duplicate entry for %s.\n",
             file_name, cur_line, line.words[0]);
      exit(EXIT_FAILURE);
    }
    env_noise_already_set = true;

    if (strcmp(line.words[1], "none") == 0)
    {
      assert(line.nb_words == 2);
      values.env_noise_method_ = NO_NOISE;
    }
    else if (strcmp(line.words[1], "FRACTAL") == 0)
    {
      assert(line.nb_words == 6);
      values.env_noise_method_ = FRACTAL;
      values.env_noise_sampling_log_ = atoi(line.words[2]);
      values.env_noise_sigma_ = atof(line.words[3]);
      values.env_noise_alpha_ = atof(line.words[4]);
      values.env_noise_prob_ = atof(line.words[5]);
      values.env_noise_seed_ = atoi(line.words[6]);
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 " : unknown phenotypic target noise method.\n",
             file_name,
             cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SECRETION_FITNESS_CONTRIB") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option, "
               "use SECRETION_CONTRIB_TO_FITNESS instead.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "SECRETION_CONTRIB_TO_FITNESS") == 0)
  {
    values.secretion_contrib_to_fitness_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SECRETION_DIFFUSION_PROP") == 0)
  {
    values.secretion_diffusion_prop_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SECRETION_DEGRADATION_PROP") == 0)
  {
    values.secretion_degradation_prop_ = atof(line.words[1]);
    if (values.secretion_degradation_prop_ > 1 || values.secretion_degradation_prop_ < 0)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": degradation must be in (0,1).\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SECRETION_INITIAL") == 0)
  {
    values.secretion_init_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SECRETION_COST") == 0)
  {
    values.secretion_cost_ = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "ALLOW_PLASMIDS") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "PLASMID_INITIAL_LENGTH") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "PLASMID_INITIAL_GENE") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "PLASMID_MINIMAL_LENGTH") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "PLASMID_MAXIMAL_LENGTH") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "CHROMOSOME_MINIMAL_LENGTH") == 0)
  {
    values.chromosome_minimal_length_ = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "CHROMOSOME_MAXIMAL_LENGTH") == 0)
  {
    values.chromosome_maximal_length_ = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "PROB_HORIZONTAL_TRANS") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "PROB_PLASMID_HT") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "TUNE_DONOR_ABILITY") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "TUNE_RECIPIENT_ABILITY") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "DONOR_COST") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "RECIPIENT_COST") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "COMPUTE_PHEN_CONTRIB_BY_GU") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32
           ": %s is no longer a valid option.\n",
           file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "LOG") == 0)
  {
    printf("LOGGING ");
    for (int8_t i = 1 ; i < line.nb_words ; i++)
    {
      if (strcmp(line.words[i], "TRANSFER") == 0)
      {
	printf("TRANSFER ");
        values.logs_ |= LOG_TRANSFER;
      }
      else if (strcmp(line.words[i], "REAR") == 0)
      {
	printf("REAR ");
        values.logs_ |= LOG_REAR;
      }
      else if (strcmp(line.words[i], "BARRIER") == 0)
      {
	printf("BARRIER ");
        values.logs_ |= LOG_BARRIER;
      }
        /*else if (strcmp(line.words[i], "LOADS") == 0)
        {
          tmp_to_be_logged |= LOG_LOADS;
        }   */
      else
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : unknown log option %s.\n",
               file_name, cur_line, line.words[1]);
        exit(EXIT_FAILURE);
      }
    }
    printf("(%d)\n", values.logs_);
  }
  else if (strcmp(line.words[0], "FUZZY_FLAVOR") == 0)
  {
    if (strcmp(line.words[1], "VECTOR") == 0) {
      values._fuzzy_flavor = FuzzyFlavor::VECTOR;
    }
    else if (strcmp(line.words[1], "DISCRETE_DOUBLE_TABLE") == 0) {
      values._fuzzy_flavor = FuzzyFlavor::DISCRETE_DOUBLE_TABLE;
    }
    else {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : unknown fuzzy flavor %s.\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }  else if (strcmp(line.words[0], "SIMD_METADATA_FLAVOR") == 0)
  {
    if (strncmp(line.words[1], "stdmap", 6) == 0)
    {
      values.simd_metadata_flavor_ = STD_MAP;
      printf("Not fully working ATM, choose list\n"); exit(-1);
    }
    else if (strncmp(line.words[1], "dyntab", 6) == 0)
    {
      values.simd_metadata_flavor_ = DYN_TAB;
      printf("Not fully working ATM, choose list\n"); exit(-1);
    }
    else if (strncmp(line.words[1], "list", 6) == 0)
    {
      values.simd_metadata_flavor_ = STD_LIST;
    }
  }
  #ifdef BASE_4
else if(strcmp(line.words[0], "AMINO_ACID") == 0)
  {
    if(!values.mwh_bases_redefined_)
    {
      for(auto i = 0; i < NB_AMINO_ACIDS; i++)
      {
        values.aa_base_m_[i] = (int8_t) -1;
        values.aa_base_w_[i] = (int8_t) -1;
        values.aa_base_h_[i] = (int8_t) -1;
      }

      values.mwh_bases_redefined_ = true;
    }

    if(line.nb_words < 3)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : amino-acid base definition should at least comprise one amino-acid and one base digit\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }

    AminoAcid amino_acid;
    if(values.str_to_aminoacid_.find(std::string(line.words[1])) != values.str_to_aminoacid_.end())
    {
      amino_acid = values.str_to_aminoacid_.at(std::string(line.words[1]));
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : unrecognized amino-acid string : %s\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }

    for(auto i = 2; i < line.nb_words; i++)
    {
      if(strlen(line.words[i]) < 2)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : malformed base digit : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      int8_t digit;

      try {
        digit = (int8_t) std::stoi(std::string(
            &(line.words[i][1])
        ));
      } catch (std::exception const &e) {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : malformed base digit (invalid value) : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      if(digit < 0)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : negative values aren't supported for base digits : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      switch(line.words[i][0])
      {
      case 'M':
        values.aa_base_m_[amino_acid] = digit;
        break;
      case 'W':
        values.aa_base_w_[amino_acid] = digit;
        break;
      case 'H':
        values.aa_base_h_[amino_acid] = digit;
        break;
      }
    }
  }
  #endif

#ifdef __REGUL
    else if (strcmp(line.words[0], "HILL_SHAPE_N") == 0)
    {
      values._hill_shape_n = atof(line.words[1]);
    }
    else if (strcmp(line.words[0], "HILL_SHAPE_THETA") == 0)
    {
      values._hill_shape_theta = atof(line.words[1]);
    }
    else if (strcmp(line.words[0], "DEGRADATION_RATE") == 0)
    {
      values._degradation_rate = atof(line.words[1]);
    }
    else if (strcmp(line.words[0], "NB_DEGRADATION_STEP") == 0)
    {
      values._nb_degradation_step = atoi(line.words[1]);
    }
    else if (strcmp(line.words[0], "NB_INDIV_AGE") == 0)
    {
      values._nb_indiv_age = atoi(line.words[1]);
    }
    else if (strcmp(line.words[0], "RANDOM_BINDING_MATRIX") == 0)
    {
        if (strncmp(line.words[1], "true", 4) == 0)
        {
          values._random_binding_matrix = true;
        }
        else if (strncmp(line.words[1], "false", 5) == 0)
        {
          values._random_binding_matrix = false;
        }
        else
        {
          printf("ERROR in param file \"%s\" on line %" PRId32 " : unknown more random_binding_matrix option (use true/false).\n",
                 file_name, cur_line);
          exit(EXIT_FAILURE);
        }
    }
    else if (strcmp(line.words[0], "BINDING_ZEROS_PERCENTAGE") == 0)
    {
      values._binding_zeros_percentage = atof(line.words[1]);
    }
    else if (strcmp(line.words[0], "INDIVIDUAL_EVALUATION_AGES") == 0)
    {
      values._list_eval_step.clear();
      for (int i = 1; i < line.nb_words; i++)
        values._list_eval_step.insert(atoi(line.words[i]));
    }
    else if (strcmp(line.words[0], "WITH_HEREDITY") == 0)
    {
      if (strncmp(line.words[1], "true", 4) == 0)
      {
        values._with_heredity = true;
      }
      else if (strncmp(line.words[1], "false", 5) == 0)
      {
        values._with_heredity = false;
      }
      else
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : unknown with_heredity option (use true/false).\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(line.words[0], "PROTEIN_PRESENCE_LIMIT") == 0)
    {
      values._protein_presence_limit = atof(line.words[1]);
    }
    else if (strcmp(line.words[0], "NB_ENVIRONMENTS") == 0)
    {
      int16_t nb_env = atoi( line.words[1] );

      if( nb_env < 1 )
      {
        printf( "ERROR in param file \"%s\" on line %" PRId32 " : you must have at least one environment\n", file_name, cur_line );
        printf("you put %" PRId16 "\n", nb_env);
        exit( EXIT_FAILURE );
      }

      // Utile uniquement en cas de reprise sur backup
      // Je ne sais pas comment ça va se passer avec cette version ...
      if( values._env_gaussians_list.size() > 0 )
      {
        values._env_gaussians_list.clear();
      }


      if( values._env_signals_list.size() > 0 )
      {
        values._env_signals_list.clear();
      }


      for( int16_t i = 0; i < nb_env; i++)
      {
        values._env_gaussians_list.push_back(std::list<Gaussian>());
        values._env_signals_list.push_back(std::list<int16_t>());
      }
    }
    else if (strcmp(line.words[0], "CREATE_SIGNAL") == 0)
    {
      int signal_lenght = line.nb_words - 1;

      std::vector<Codon*> codons;
      Codon* codon = NULL;
      for (int8_t i = 0; i < signal_lenght; i++)
      {
        if(strcmp(line.words[i+1], "h0")==0)
        {
          codon = new Codon(CODON_H0);
        }
        else if(strcmp(line.words[i+1], "h1")==0)
        {
          codon = new Codon(CODON_H1);
        }
        else if(strcmp(line.words[i+1], "w0")==0)
        {
          codon = new Codon(CODON_W0);
        }
        else if(strcmp(line.words[i+1], "w1")==0)
        {
          codon = new Codon(CODON_W1);
        }
        else if(strcmp(line.words[i+1], "m0")==0)
        {
          codon = new Codon(CODON_M0);
        }
        else if(strcmp(line.words[i+1], "m1")==0)
        {
          codon = new Codon(CODON_M1);
        }
        else
        {
          printf("Error this codon doesn't exist\n");
          exit( EXIT_FAILURE );
        }
        codons.push_back(codon);
      }
      values._signals_models.push_back(new Protein_R(codons, 0.5, values.w_max_));

      for (auto cod : codons) delete cod;

      codons.clear();
    }
    else if (strcmp(line.words[0], "ENV_ADD_SIGNAL") == 0)
    {
      // le premier chiffre est l'indice d'environment en convention humaine ( le premier a 1)
      // On vérifie que cet indice n'est pas trop élevé ni négatif pour éviter les crash
      if ( atoi(line.words[1]) - 1 < (int)values._env_signals_list.size() && atoi(line.words[1]) > 0)
      {
        (values._env_signals_list.at( atoi(line.words[1]) - 1)).push_back(atoi(line.words[2]) - 1);
      }
      else
      {
        printf( " ERROR in param file \"%s\" on line %" PRId32 " : There are only %ld environment.\n",
         file_name, cur_line, values._env_gaussians_list.size() );
        exit( EXIT_FAILURE );
      }
    }
  #endif

  else
  {
    printf("ERROR in param file \"%s\" on line %" PRId32 " : undefined key word \"%s\"\n", file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
}

} // namespace aevol
