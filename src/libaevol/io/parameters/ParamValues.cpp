// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ParamValues.h"

#include <cstdlib>
#include <cstring>

#include "ae_enums.h"

#ifndef LOGIN_NAME_MAX
  #define LOGIN_NAME_MAX 256
#endif

namespace aevol {

ParamValues::ParamValues() {
  // ----------------------------------------- PseudoRandom Number Generators
  seed_           = 0;
  mut_seed_       = 0;
  stoch_seed_     = 0;
  env_var_seed_   = 0;
  env_noise_seed_ = 0;

  // ------------------------------------------------------------ Constraints
  min_genome_length_  = 1;
  max_genome_length_  = 10000000;
  chromosome_minimal_length_  = -1;
  chromosome_maximal_length_  = -1;
  w_max_              = 0.033333333;

  // ----------------------------------------------------- Initial conditions
  chromosome_initial_length_  = 5000;
  init_method_            = ONE_GOOD_GENE | CLONE;
  init_pop_size_          = 1024;
  strain_name_ = new char[STRAIN_NAME_DEFAULT_SIZE+1];

  for (int i = 0; i < STRAIN_NAME_DEFAULT_SIZE+1; i++)
    strain_name_[i] = 0;

  // ------------------------------------------------------------- Strain name
  char* login_name = new char[LOGIN_NAME_MAX+1];
  // Try get user login. If fail, replace by default value
  if(getlogin_r(login_name, LOGIN_NAME_MAX) != 0)
    strcpy(login_name, "anon");

  // Copy login into strain name with at most STRAIN_NAME_LOGIN_SIZE characters
  strncpy(strain_name_, login_name, STRAIN_NAME_LOGIN_SIZE);
  delete [] login_name;

  // Null-terminate the c-string if the max number of characters were copied
  if (strain_name_[STRAIN_NAME_LOGIN_SIZE] != 0)
    strain_name_[STRAIN_NAME_LOGIN_SIZE + 1] = 0;

  // -------------------------------------------------------- Phenotypic target
  env_sampling_ = 300;

  // ------------------------------------ Phenotypic target x-axis segmentation
  env_axis_nb_segments_         = 1;
  env_axis_segment_boundaries_  = NULL;
  env_axis_features_            = NULL;
  env_axis_separate_segments_   = false;

  // ---------------------------------------------- Phenotypic target variation
  env_var_method_ = NO_VAR;
  env_var_sigma_  = 0;
  env_var_tau_    = 0;

  // -------------------------------------------------- Phenotypic target noise
  env_noise_method_       = NO_NOISE;
  env_noise_alpha_        = 0;
  env_noise_sigma_        = 0;
  env_noise_prob_         = 0;
  env_noise_sampling_log_ = 0;

  // --------------------------------------------------------- Mutation rates
  point_mutation_rate_  = 1e-5;
  small_insertion_rate_ = 1e-5;
  small_deletion_rate_  = 1e-5;
  max_indel_size_       = 6;

  // -------------------------------------------- Rearrangements and Transfer
  with_4pts_trans_            = true;
  with_alignments_            = false;
  with_HT_                    = false;
  repl_HT_with_close_points_  = false;
  HT_ins_rate_                = 0.0;
  HT_repl_rate_               = 0.0;
  repl_HT_detach_rate_        = 0.0;

  // ------------------------------ Rearrangement rates (without alignements)
  duplication_rate_   = 1e-5;
  deletion_rate_      = 1e-5;
  translocation_rate_ = 1e-5;
  inversion_rate_     = 1e-5;

  // --------------------------------- Rearrangement rates (with alignements)
  neighbourhood_rate_       = 5e-5;
  duplication_proportion_   = 0.3;
  deletion_proportion_      = 0.3;
  translocation_proportion_ = 0.3;
  inversion_proportion_     = 0.3;

  // ------------------------------------------------------------- Alignments
  align_fun_shape_    = SIGMOID;
  align_sigm_lambda_  = 4;
  align_sigm_mean_    = 50;
  align_lin_min_      = 0;
  align_lin_max_      = 100;

  align_max_shift_      = 20;
  align_w_zone_h_len_   = 50;
  align_match_bonus_    = 1;
  align_mismatch_cost_  = 2;

  // ----------------------------------------------- Phenotypic Stochasticity
  with_stochasticity_ = false;

  // -------------------------------------------------------------- Selection
  selection_scheme_   = RANK_EXPONENTIAL;
  selection_pressure_ = 0.998;

  selection_scope_   = SCOPE_LOCAL;
  selection_scope_x_ = 3;
  selection_scope_y_ = 3;

  fitness_function_ = FITNESS_EXP;
  fitness_function_x_ = 3;
  fitness_function_y_ = 3;
  // -------------------------------------------------------------- Secretion
  with_secretion_               = false;
  secretion_contrib_to_fitness_ = 0;
  secretion_diffusion_prop_     = 0;
  secretion_degradation_prop_   = 0;
  secretion_cost_               = 0;
  secretion_init_               = 0;

  // ------------------------------------------------------- Translation cost
  translation_cost_ = 0;

#ifdef BASE_4
  // ------------------------------------------------------------ Terminators
  term_polya_seq_length_ = 0;
#endif

  // ---------------------------------------------------------------- Outputs
  stats_            = 0;
  delete_old_stats_ = false;

  // Backups
  backup_step_      = 500;
  big_backup_step_  = 10000;

  // Tree
  record_tree_  = false;
  tree_step_    = 100;

  //LightTree
  record_light_tree_ = false;

  // Dumps
  make_dumps_ = false;
  dump_step_  = 1000;

  // Logs
  logs_ = 0;

  // Other
  more_stats_ = false;

  _fuzzy_flavor = FuzzyFlavor::VECTOR;

  simd_metadata_flavor_ = MetadataFlavor::STD_LIST;

#ifdef __REGUL
  // ------------------------------------------------------- Binding matrix
  _binding_zeros_percentage = 75;

  _protein_presence_limit = 1e-2;
  _degradation_rate  = 1;
  _nb_degradation_step  = 10;
  _nb_indiv_age         = 20;
  _with_heredity          = false;

  _hill_shape_n      = 4;
  _hill_shape_theta  = 0.5;
  _hill_shape        = std::pow( _hill_shape_theta, _hill_shape_n );

  _list_eval_step.insert(_nb_indiv_age);
  _env_switch_probability = 0.1;
#endif
}

ParamValues::~ParamValues() {
  delete [] env_axis_segment_boundaries_;
  delete [] env_axis_features_;
  delete [] strain_name_;
}

void ParamValues::print_to_file(FILE* file)
{
  // ------------------------------------------------------------ Constraints
  fprintf(file, "\nConstraints ---------------------------------------------\n");
  fprintf(file, "min_genome_length :          %" PRId32 "\n", min_genome_length_);
  fprintf(file, "max_genome_length :          %" PRId32 "\n", max_genome_length_);
  fprintf(file, "chromosome_minimal_length :  %" PRId32 "\n", chromosome_minimal_length_);
  fprintf(file, "chromosome_maximal_length :  %" PRId32 "\n", chromosome_maximal_length_);
  fprintf(file, "W_MAX :                      %f\n",        w_max_);

  // --------------------------------------------------------- Mutation rates
  fprintf(file, "\nMutation rates ------------------------------------------\n");
  fprintf(file, "point_mutation_rate :        %e\n",  point_mutation_rate_);
  fprintf(file, "small_insertion_rate :       %e\n",  small_insertion_rate_);
  fprintf(file, "small_deletion_rate :        %e\n",  small_deletion_rate_);
  fprintf(file, "max_indel_size :             %" PRId16 "\n", max_indel_size_);

  // -------------------------------------------- Rearrangements and Transfer
  fprintf(file, "\nRearrangements and Transfer -----------------------------\n");
  fprintf(file, "with_4pts_trans :            %s\n",  with_4pts_trans_? "true" : "false");
  fprintf(file, "with_alignments :            %s\n",  with_alignments_? "true" : "false");
  fprintf(file, "with_HT :                    %s\n",  with_HT_? "true" : "false");
  fprintf(file, "repl_HT_with_close_points :  %s\n",  repl_HT_with_close_points_? "true" : "false");
  fprintf(file, "HT_ins_rate :                %e\n",  HT_ins_rate_);
  fprintf(file, "HT_repl_rate :               %e\n",  HT_repl_rate_);

  // ---------------------------------------------------- Rearrangement rates
  if (with_alignments_)
  {
    fprintf(file, "\nRearrangement rates (with alignements) ------------------\n");
    fprintf(file, "neighbourhood_rate :         %e\n",  neighbourhood_rate_);
    fprintf(file, "duplication_proportion :     %e\n",  duplication_proportion_);
    fprintf(file, "deletion_proportion :        %e\n",  deletion_proportion_);
    fprintf(file, "translocation_proportion :   %e\n",  translocation_proportion_);
    fprintf(file, "inversion_proportion :       %e\n",  inversion_proportion_);
  }
  else
  {
    fprintf(file, "\nRearrangement rates (without alignements) ----------------\n");
    fprintf(file, "duplication_rate :           %e\n",  duplication_rate_);
    fprintf(file, "deletion_rate :              %e\n",  deletion_rate_);
    fprintf(file, "translocation_rate :         %e\n",  translocation_rate_);
    fprintf(file, "inversion_rate :             %e\n",  inversion_rate_);
  }

  // ------------------------------------------------------------ Alignements
  fprintf(file, "\nAlignements ---------------------------------------------\n");
  fprintf(file, "align_fun_shape :            %" PRId16 "\n", (int16_t) align_fun_shape_);
  fprintf(file, "align_sigm_lambda :          %f\n",        align_sigm_lambda_);
  fprintf(file, "align_sigm_mean :            %" PRId16 "\n", align_sigm_mean_);
  fprintf(file, "align_lin_min :              %" PRId16 "\n", align_lin_min_);
  fprintf(file, "align_lin_max :              %" PRId16 "\n", align_lin_max_);
  fprintf(file, "align_max_shift :            %" PRId16 "\n", align_max_shift_);
  fprintf(file, "align_w_zone_h_len :         %" PRId16 "\n", align_w_zone_h_len_);
  fprintf(file, "align_match_bonus :          %" PRId16 "\n", align_match_bonus_);
  fprintf(file, "align_mismatch_cost :        %" PRId16 "\n", align_mismatch_cost_);

  // -------------------------------------------------------------- Selection
  fprintf(file, "\nSelection -----------------------------------------------\n");
  switch (selection_scheme_)
  {
  case RANK_LINEAR :
  {
    fprintf(file, "selection_scheme :           RANK_LINEAR\n");
    break;
  }
  case RANK_EXPONENTIAL :
  {
    fprintf(file, "selection_scheme :           RANK_EXPONENTIAL\n");
    break;
  }
  case FITNESS_PROPORTIONATE :
  {
    fprintf(file, "selection_scheme :           FITNESS_PROPORTIONATE\n");
    break;
  }
  case FITTEST :
  {
    fprintf(file, "selection_scheme :           FITTEST\n");
    break;
  }
  default :
  {
    fprintf(file, "selection_scheme :           UNKNOWN\n");
    break;
  }
  }
  fprintf(file, "selection_pressure :         %e\n",  selection_pressure_);

  switch (selection_scope_)
  {
  case SCOPE_GLOBAL :
  {
    fprintf(file, "selection_scope :           GLOBAL\n");
    break;
  }
  case SCOPE_LOCAL :
  {
    fprintf(file, "selection_scope :          LOCAL %d %d\n", selection_scope_x_, selection_scope_y_);
    break;
  }
  default :
  {
    fprintf(file, "selection_scope :           UNKNOWN\n");
    break;
  }
  }

  // -------------------------------------------------------------- Secretion
  fprintf(file, "\nSecretion -----------------------------------------------\n");
  fprintf(file, "with_secretion :                %s\n", with_secretion_? "true" : "false");
  fprintf(file, "secretion_contrib_to_fitness :  %e\n", secretion_contrib_to_fitness_);
  fprintf(file, "secretion_diffusion_prop :      %e\n", secretion_diffusion_prop_);
  fprintf(file, "secretion_degradation_prop :    %e\n", secretion_degradation_prop_);
  fprintf(file, "secretion_cost :                %e\n", secretion_cost_);

  // ------------------------------------------------------- Translation cost
  fprintf(file, "\nTranslation cost ----------------------------------------\n");
  fprintf(file, "translation_cost :           %e\n",  translation_cost_);
}

void ParamValues::CheckConsistency() {
  if (chromosome_maximal_length_ == -1)
    chromosome_maximal_length_ = max_genome_length_;
  if (chromosome_minimal_length_ == -1)
    chromosome_minimal_length_ = min_genome_length_;
  if (chromosome_minimal_length_ > chromosome_initial_length_) {
    printf("ERROR: CHROMOSOME_INITIAL_LENGTH is lower than CHROMOSOME_MINIMAL_LENGTH\n");
    exit(EXIT_FAILURE);
  }
  if (chromosome_maximal_length_ < chromosome_initial_length_) {
    printf("ERROR: CHROMOSOME_INITIAL_LENGTH is higher than CHROMOSOME_MAXIMAL_LENGTH\n");
    exit(EXIT_FAILURE);
  }
  // Check that the population fits in the spatial structure
  if (init_pop_size_ != grid_width_ * grid_height_)
  {
    printf("ERROR: the number of individuals (%" PRId32
           ") does not match the size of the grid  (%" PRId16
           " * %" PRId16 ")\n",
           init_pop_size_,
           grid_width_,
           grid_height_);
    exit(EXIT_FAILURE);
  }
}

} // namespace aevol
