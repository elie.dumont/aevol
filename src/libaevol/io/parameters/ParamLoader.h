// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PARAM_LOADER_H_
#define AEVOL_PARAM_LOADER_H_

// =================================================================
//                              Includes
// =================================================================
#include <cinttypes>

#include "legacy/ExpManager.h"
#include "ParamValues.h"

namespace aevol {

class ParamLoader {
 public:
  // =========================================================================
  //                          Constructors & Destructor
  // =========================================================================
  ParamLoader()                              = delete;
  ParamLoader(const ParamLoader&)            = delete;
  ParamLoader(ParamLoader&&)                 = delete;
  virtual ~ParamLoader()                     = delete;

  // ==========================================================================
  //                                Operators
  // ==========================================================================
  ParamLoader& operator=(const ParamLoader& other) = delete;
  ParamLoader& operator=(ParamLoader&& other) = delete;

  // =========================================================================
  //                             Public Methods
  // =========================================================================
  static void load(const ParamValues& param_values,
                   ExpManager* exp_m,
                   bool verbose = false,
                   char* chromosome = nullptr,
                   int32_t lchromosome = 0
                   #ifdef HAVE_MPI
                   , int32_t nb_rank = 1
                   #endif
  );

 protected:
};

}  // namespace aevol

#endif  // AEVOL_PARAMLOADER_H_
